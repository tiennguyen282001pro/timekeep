<?php

namespace App\View\Components\Erp;

use App\Models\Emodule;
use Illuminate\Support\Str;
use Illuminate\View\Component;
use Nwidart\Modules\Facades\Module;

class HeaderBar extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $search_btn;
    public $module;
    public function __construct($module)
    {
        $this->module = $module;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data = Emodule::where('active',1)
            ->where('parent_id','!=' ,'0')
            ->where('name','like',"%n%")
            ->get()->pluck('name');
        $emodule  = Emodule::where("active",1)->get();
        $ruslt = [];

        foreach ($emodule as $key => $value)
        {
            if ($value->parent_id == 0)
            {
                $ruslt[$value->id]["title"] = $value->name;
                $id[] = $value["id"];
            }
            if ($value->parent_id != 0)
            {
                foreach ($id as $val)
                {
                    if ($value->parent_id == $val)
                    {
                        $ruslt[$val]["child"][] = $value;
                    }
                }
            }

        }


        return view('components.erp.header-bar',compact("ruslt"));
    }

}
