<?php

namespace App\Models;

use App\Casts\BooleanCast;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\StringCast;
use App\Casts\SlugCast;
use App\Casts\IntegerCast;

class Emodule extends Model
{
    use HasFactory;

    protected $table = 'emodules';

    protected $fillable = ["name", "label", "slug", "icon", "color", "permission", "parent_id", "active", "type"];

    public static $listFields = ["id", "name", "label", "slug", "icon", "color", "permission", "parent_id", "active", "type", "created_at", "updated_at"];

    protected $casts = [
        "name" => StringCast::class,
		"label" => StringCast::class,
		"icon" => StringCast::class,
		"color" => StringCast::class,
		"permission" => StringCast::class,
		"parent_id" => IntegerCast::class,
		"active" => BooleanCast::class,
		"type" => IntegerCast::class,
    ];

    public function parent(){
        return $this->belongsTo(Emodule::class,"parent_id","id");
    }
}
