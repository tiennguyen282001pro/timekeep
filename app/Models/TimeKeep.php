<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\IntegerCast;
use App\Casts\StringCast;

class TimeKeep extends Model
{
    use HasFactory;

    protected $table = 'time_keeps';

    protected $fillable = ["user_id", "time", "company_id", "status", "note"];

    public static $listFields = ["id", "user_id", "time", "company_id", "status", "note", "created_at", "updated_at"];

    protected $casts = [
        "user_id" => IntegerCast::class,
		"company_id" => IntegerCast::class,
		"status" => IntegerCast::class,
		"note" => StringCast::class,
		
    ];
}
