<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\IntegerCast;
use App\Casts\StringCast;
use App\Casts\BooleanCast;

class Notify extends Model
{
    use HasFactory;

    protected $table = 'notifies';

    protected $fillable = ["recipient", "sender", "content", "type", "status"];

    public static $listFields = ["id", "recipient", "sender", "content", "type", "status", "created_at", "updated_at"];

    protected $casts = [
        "recipient" => IntegerCast::class,
		"sender" => IntegerCast::class,
		"content" => StringCast::class,
		"type" => BooleanCast::class,
		"status" => BooleanCast::class,

    ];
    public function user(){
        return $this->belongsTo(User::class,"recipient","id");
    }
    public function senders(){
        return $this->belongsTo(User::class,"sender","id");
    }
}
