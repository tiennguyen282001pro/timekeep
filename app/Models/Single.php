<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Casts\IntegerCast;
use App\Casts\StringCast;

class Single extends Model
{
    use HasFactory;

    protected $table = 'singles';

    protected $fillable = ["user_id", "name", "company_id", "type", "value", "reason", "censor", "status"];

    public static $listFields = ["id", "user_id", "name", "company_id", "type", "value", "reason", "censor", "status", "created_at", "updated_at"];

    protected $casts = [
        "user_id" => IntegerCast::class,
		"name" => StringCast::class,
		"company_id" => IntegerCast::class,
		"type" => IntegerCast::class,
		"censor" => StringCast::class,
		"status" => IntegerCast::class,
		
    ];
}
