@props(['title'=>'Company'])
<x-erp-layout module="company" :title="$title">
    <x-slot:assets>

    </x-slot:assets>
    {{$slot}}
</x-erp-layout>
