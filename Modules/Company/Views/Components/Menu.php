<?php

namespace Modules\Company\Views\Components;

use Illuminate\View\Component;

class Menu extends Component
{

    public function render()
    {
        $data = config("company.menu",[]);

        return view("company::components.menu", compact("data"));
    }
}
