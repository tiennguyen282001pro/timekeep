@props(['title'=>'Personnel'])
<x-erp-layout module="personnel" :title="$title">
    <x-slot:assets>

    </x-slot:assets>
    {{$slot}}
</x-erp-layout>
