<?php

namespace Modules\Personnel\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function index()
    {
        lForm()->setTitle('Personnel');
        lForm()->pushBreadCrumb(route('personnel'),'Personnel');

        return view('personnel::index');
    }
}
