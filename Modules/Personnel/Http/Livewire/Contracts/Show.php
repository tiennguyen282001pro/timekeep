<?php

namespace Modules\Personnel\Http\Livewire\Contracts;

use App\Models\Contract;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("personnel.contracts.show");
        Contract::findOrFail($this->record_id);
    }

    public function render()
    {
        $data =  Contract::findOrFail($this->record_id);
        lForm()->setTitle("Hợp đồng");
        lForm()->pushBreadcrumb(route("personnel"), "personnel");
        lForm()->pushBreadcrumb(route("personnel.contracts"), "Contracts");
        lForm()->pushBreadcrumb(route("personnel.contracts.show", $this->record_id), "Show");

        return view("personnel::livewire.contracts.show", compact("data"))
            ->layout('personnel::layouts.master', ['title' => 'Contracts Show']);
    }
}
