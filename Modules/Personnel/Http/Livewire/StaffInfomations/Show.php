<?php

namespace Modules\Personnel\Http\Livewire\StaffInfomations;

use App\Models\StaffInfomation;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("personnel.staff-infomations.show");
        StaffInfomation::findOrFail($this->record_id);
    }

    public function render()
    {
        $data =  StaffInfomation::findOrFail($this->record_id);
        lForm()->setTitle("Thông tin nhân viên");
        lForm()->pushBreadcrumb(route("personnel"), "personnel");
        lForm()->pushBreadcrumb(route("personnel.staff-infomations"), "Staff Infomations");
        lForm()->pushBreadcrumb(route("personnel.staff-infomations.show", $this->record_id), "Show");

        return view("personnel::livewire.staff-infomations.show", compact("data"))
            ->layout('personnel::layouts.master', ['title' => 'Staff Infomations Show']);
    }
}
