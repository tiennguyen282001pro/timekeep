<?php

namespace Modules\Personnel\Http\Livewire\Users;

use App\Models\Personnel;
use App\Models\Department;
use App\Models\Position;
use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("personnel.users.show");
        User::findOrFail($this->record_id);
    }

    public function render()
    {
        $data =  User::findOrFail($this->record_id);
        lForm()->setTitle("Nhân viên");
        lForm()->pushBreadcrumb(route("personnel"), "personnel");
        lForm()->pushBreadcrumb(route("personnel.users"), "Users");
        lForm()->pushBreadcrumb(route("personnel.users.show", $this->record_id), "Show");

        $companies = personnel::all();
        $departments = Department::all();
        $positions = Position::all();

        return view("personnel::livewire.users.show", compact("data", 'companies', 'departments', 'positions'))
            ->layout('personnel::layouts.master', ['title' => 'Users Show']);
    }
}
