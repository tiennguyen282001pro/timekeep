<?php
Route::get('/', [\Modules\Personnel\Http\Controllers\HomeController::class,"index"])->can('personnel');

//---END-OF-ROUTES---//

Route::prefix("users")->name(".users")->group(function () {
    Route::get("/", \Modules\Personnel\Http\Livewire\Users\Listing::class)->can("personnel.users");
    Route::get("/create/{record_id}", \Modules\Personnel\Http\Livewire\Users\Create::class)->name(".create")->can("personnel.users.create");
    Route::get("/edit/{record_id}", \Modules\Personnel\Http\Livewire\Users\Edit::class)->name(".edit")->can("personnel.users.edit");
    Route::get("/show/{record_id}", \Modules\Personnel\Http\Livewire\Users\Show::class)->name(".show")->can("personnel.users.edit");
    //---END-OF-USERS---//
});

Route::prefix("contracts")->name(".contracts")->group(function () {
    Route::get("/", \Modules\Personnel\Http\Livewire\Contracts\Listing::class)->can("personnel.contracts");
    Route::get("/create/{record_id}", \Modules\Personnel\Http\Livewire\Contracts\Create::class)->name(".create")->can("personnel.contracts.create");
    Route::get("/edit/{record_id}", \Modules\Personnel\Http\Livewire\Contracts\Edit::class)->name(".edit")->can("personnel.contracts.edit");
    Route::get("/show/{record_id}", \Modules\Personnel\Http\Livewire\Contracts\Show::class)->name(".show")->can("personnel.contracts.show");
    //---END-OF-CONTRACTS---//
});

Route::prefix("staff-infomations")->name(".staff-infomations")->group(function () {
    Route::get("/", \Modules\Personnel\Http\Livewire\StaffInfomations\Listing::class)->can("personnel.staff-infomations");
    Route::get("/create", \Modules\Personnel\Http\Livewire\StaffInfomations\Create::class)->name(".create")->can("personnel.staff-infomations.create");
    Route::get("/edit/{record_id}", \Modules\Personnel\Http\Livewire\StaffInfomations\Edit::class)->name(".edit")->can("personnel.staff-infomations.edit");
    Route::get("/show/{record_id}", \Modules\Personnel\Http\Livewire\StaffInfomations\Show::class)->name(".show")->can("personnel.staff-infomations.show");
    //---END-OF-STAFFINFOMATIONS---//
});
