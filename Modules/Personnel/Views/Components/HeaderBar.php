<?php

namespace Modules\Personnel\Views\Components;



use Illuminate\View\Component;

class HeaderBar extends Component
{

    public function render(){

        return view('personnel::components.header-bar');
    }
}
