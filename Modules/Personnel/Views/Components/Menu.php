<?php

namespace Modules\Personnel\Views\Components;

use Illuminate\View\Component;

class Menu extends Component
{

    public function render()
    {
        $data = config("personnel.menu",[]);

        return view("personnel::components.menu", compact("data"));
    }
}
