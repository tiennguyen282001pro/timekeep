<?php

namespace Modules\Personnel\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try{
            $data = config("personnel.permission",[]);
            foreach($data as $per){
                Gate::define($per, function ($user) use ($per) {
                    return $user->hasPermissionTo($per);
                });
            }
        } catch (\Exception $e) {
            //  report($e);
            return false;
        }
    }
}
