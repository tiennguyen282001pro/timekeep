<?php

namespace Modules\Admin\Http\Livewire\Notifies;

use App\Models\Notify;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("admin.notifies.show");
    }

    public function render()
    {
        $data =  Notify::findOrFail($this->record_id);
        lForm()->setTitle("Notifies Show");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.notifies"),"Notifies");
		lForm()->pushBreadcrumb(route("admin.notifies.show",$this->record_id),"Show");

        return view("admin::livewire.notifies.show", compact("data"))
            ->layout('admin::layouts.master');
    }
}
