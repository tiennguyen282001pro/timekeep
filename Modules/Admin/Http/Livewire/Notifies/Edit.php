<?php

namespace Modules\Admin\Http\Livewire\Notifies;

use App\Models\Notify;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Edit extends Component
{
    use WithLaravelFormTrait;

    public $recipient, $sender, $content, $type, $status;

    protected function rules()
    {
        return [
            'recipient' => '',
			'sender' => '',
			'content' => '',
			'type' => '',
			'status' => '',

        ];
    }

    public function mount()
    {
        $this->authorize("admin.notifies.edit");
        $data = Notify::findOrFail($this->record_id);
        $this->recipient = $data->recipient;
		$this->sender = $data->sender;
		$this->content = $data->content;
		$this->type = $data->type;
		$this->status = $data->status;

    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("admin.notifies.edit");
        $this->validate();
        $data = Notify::findOrFail($this->record_id);
        $data->fill([
            'recipient' => $this->recipient,
			'sender' => $this->sender,
			'content' => $this->content,
			'type' => $this->type,
			'status' => $this->status,

        ]);
        if (!$data->clean) {
            $data->update();
            $this->redirectForm("admin.notifies", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Sửa Thông báo");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.notifies"),"Notifies");
		lForm()->pushBreadcrumb(route("admin.notifies.edit",$this->record_id),"Edit");

        return view("admin::livewire.notifies.edit")
            ->layout('admin::layouts.master');
    }
}
