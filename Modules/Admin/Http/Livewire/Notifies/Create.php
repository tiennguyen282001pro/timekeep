<?php

namespace Modules\Admin\Http\Livewire\Notifies;

use App\Models\Notify;
use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class   Create extends Component
{
    use WithLaravelFormTrait;

    public $recipient, $sender, $content, $type, $status;
    protected $rules = [
        'recipient' => 'required',
		'sender' => 'required',
		'content' => '',
		'type' => '',
		'status' => '',

    ];
    protected $messages = [
        'recipient.required' => 'Bạn chưa chọn người nhận',
        'sender.required' => 'Bạn chưa chọn người gửi',
    ];

    public function mount()
    {
        $this->authorize("admin.notifies.create");
        $this->done = 1;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("admin.notifies.create");
        $this->validate();
        $data = Notify::create([
            'recipient' => $this->recipient,
			'sender' => $this->sender,
			'content' => $this->content,
			'type' => $this->type,
			'status' => $this->status,

        ]);
        if ($data) {
            $this->redirectForm("admin.notifies", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Thêm mới thông báo");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.notifies"),"Notifies");
		lForm()->pushBreadcrumb(route("admin.notifies.create"),"Create");
        $list_sender = User::all()->pluck('name','id');
        return view("admin::livewire.notifies.create",compact('list_sender'))
            ->layout('admin::layouts.master');
    }
}
