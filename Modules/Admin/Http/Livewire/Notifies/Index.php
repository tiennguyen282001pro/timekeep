<?php

namespace Modules\Admin\Http\Livewire\Notifies;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Notifies");
       lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.notifies"),"Notifies");
		

        return view("admin::livewire.notifies.index")
            ->layout('admin::layouts.master', ['title' => 'Notifies']);
    }
}
