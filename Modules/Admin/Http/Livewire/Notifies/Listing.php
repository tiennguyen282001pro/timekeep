<?php

namespace Modules\Admin\Http\Livewire\Notifies;

use App\Models\Notify;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\WithPagination;
use Livewire\Component;

class Listing extends Component
{
    use WithLaravelFormTrait;
     use  WithPagination;

    public $confirm = 0;
    // Filter
    public $fId;
    // Sort
    public $sId = 0;
    public $fields = [
        "id" => ["status" => true, "label" => "Id"],
		"recipient" => ["status" => true, "label" => "Recipient"],
		"sender" => ["status" => true, "label" => "Sender"],
		"content" => ["status" => true, "label" => "Content"],
		"type" => ["status" => true, "label" => "Type"],
		"status" => ["status" => true, "label" => "Status"],
		"created_at" => ["status" => true, "label" => "Created At"],
		"updated_at" => ["status" => true, "label" => "Updated At"],

    ];
    public function changeStatus($record_id)
    {
        $data = Notify::findOrFail($record_id);
        if ($data)
        {
            $data->update([
                'status' => !$data->status,
            ]);
        }

    }

    public function mount()
    {
        $this->authorize("admin.notifies.listing");
    }

    public function delete()
    {
        $this->authorize("admin.notifies.delete");
        if ($this->confirm > 0) {
            Notify::destroy($this->confirm);
        }
        $this->confirm = 0;
        $this->dispatchBrowserEvent('warning', 'Notifies successfully destroyed.');
    }

    public function render()
    {
        $data = new Notify();

        if ($this->fId > 0) {
            $data = $data->whereId($this->fId);
        }
        if ($this->sId == 1) {
            $data = $data->orderBy("id");
        }
        if ($this->sId == 2) {
            $data = $data->orderByDesc("id");
        }
        $data = $data->paginate(30);

        lForm()->setTitle("Danh sách thông báo");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.notifies"),"Notifies");
		lForm()->pushBreadcrumb(route("admin.notifies.listing"),"Listing");
        return view("admin::livewire.notifies.listing", compact("data"))
            ->layout('admin::layouts.master');
    }
}
