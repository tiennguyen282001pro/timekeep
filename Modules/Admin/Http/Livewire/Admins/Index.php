<?php

namespace Modules\Admin\Http\Livewire\Admins;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Admins");
       lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.admins"),"Admins");


        return view("admin::livewire.admins.index")
            ->layout('admin::layouts.master');
    }
}
