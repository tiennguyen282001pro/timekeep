<?php

namespace Modules\Admin\Http\Livewire\Admins;

use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("admin.admins.show");
    }

    public function render()
    {
        $data =  User::whereIsAdmin(true)->where("is_super_admin","<=",auth()->user()->is_super_admin)->with("roles")->findOrFail($this->record_id);
        lForm()->setTitle("Admins Show");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.admins"),"Admins");
		lForm()->pushBreadcrumb(route("admin.admins.show",$this->record_id),"Show");

        return view("admin::livewire.admins.show", compact("data"))
            ->layout('admin::layouts.master');
    }
}
