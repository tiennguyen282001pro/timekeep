<?php
namespace Modules\Admin\Http\Livewire\Menus;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Livewire\Component;
use Nwidart\Modules\Facades\Module;

class Listing extends Component
{
    use WithLaravelFormTrait;

    public $create ;
    public $edit;
    protected $queryString = ["create","edit"];
    public function mount(){

    }

    public function delete($record){
        $this->onlyLocalhost();
        $data = config("admin.menu",[]);
        Arr::forget($data,$record);
        $this->saveNavbar('admin',$data);
        session()->flash('message','done');
        $this->redirect(route("admin.menus"));
    }

    public function render(){
        $data = config("admin.menu",[]);

        lForm()->setTitle("Menu");
        lForm()->pushBreadCrumb(route("admin"),"Admin");
        lForm()->pushBreadCrumb(route("admin.menus"),"Menu");

        return view("admin::livewire.menus.listing",compact("data"))
            ->layout('admin::layouts.master', ['title' => 'Menu']);
    }
}
