<?php

namespace Modules\Admin\Http\Livewire\Menus;

use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Livewire\Component;
use Nwidart\Modules\Facades\Module;


class Create extends Component
{
    use WithLaravelFormTrait;

    public $module, $route, $label, $icon, $permission, $parent_id = -1, $sort = -1, $children = ["listing","create"];
    protected $rules = [
        "permission" => "string|required"
        , "route" => "string|required"
        , "label" => "string|required"
    ];

    public function mount()
    {
        $this->onlyLocalhost();
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function updatedRoute(){
       $this->permission = $this->route;
        $this->validateOnly("permission");
    }

    public function store()
    {
        $this->onlyLocalhost();
        $navbars = config( 'admin.menu', []);
        $data = [
            "label" => $this->label
            , "icon" => $this->icon
            , "route" => $this->route
            , "permission" => $this->permission
            , "children" => []
        ];
        if ($this->parent_id == -1) {
            $siblings = $navbars;
            foreach($this->children as $child){
                switch ($child){
                    case "listing":
                        $data["children"][] = [
                            "label" =>"Listing"
                            , "icon" => "list"
                            , "route" => $this->route.".listing"
                            , "permission" => $this->permission . ".lsiting"
                            , "children" => []
                        ];
                        break;
                    case "create":
                        $data["children"][] = [
                            "label" =>"Create"
                            , "icon" => "add"
                            , "route" => $this->route.".create"
                            , "permission" => $this->permission . ".create"
                            , "children" => []
                        ];
                        break;
                }
            }
        } else {
            $siblings = data_get($navbars, $this->parent_id . ".children", []);
        }

        if ($this->sort == -1) {
            $siblings = Arr::prepend($siblings, $data);
        } else {
            $temp = [];
            foreach ($siblings as $k => $item) {
                $temp[] = $item;
                if ($k == $this->sort) {
                    $temp[] = $data;
                }

            }
            $siblings = $temp;
        }
        if ($this->parent_id == -1) {
            $navbars = $siblings;
        } else {
            $navbars[$this->parent_id]["children"] = $siblings;
        }
        $this->saveNavbar('admin', $navbars);
        session()->flash('message', 'done');
        return $this->redirect(route("admin.menus"));
    }

    public function render()
    {
        $parents = [-1 => "ROOT"];
        $sorts = [-1 => "First"];
        $routes = ["" => "Select Route"];
        $permissions = ["" => "Select Permission"];
        $allNav = [];

        $navbars = config( 'admin.menu', []);
        foreach ($navbars as $k => $item) {
            $parents[$k] = $item["label"];
            $allNav[$item["route"]] = $item["route"];

            foreach ($item["children"] as $child) {
                $allNav[$child["route"]] = $child["route"];
            }
        }
        if ($this->parent_id == -1) {
            $siblings = $navbars;
        } else {
            $siblings = data_get($navbars, $this->parent_id . ".children", []);
        }
        foreach ($siblings as $k => $sibling) {
            $sorts[$k] = "After  " . $sibling["label"];
        }

        $prefix = 'admin';
        foreach (Route::getRoutes() as $route) {
            if (Str::is($prefix . '.*', $route->getName())) {
                $name = $route->getName();
                if (!in_array($name, $allNav)
                    && !Str::contains($route->uri,"{")
                    && $name != "admin.menus"
                    && $name != "admin.icons"
                    && $name != "admin.permission-configs"
                ) {
                    foreach ($route->middleware() as $mid) {
                        if (Str::is("can:" . $prefix . ".*", $mid)) {
                            $permission = Str::after($mid, "can:");
                            $permissions[$permission] = $permission;
                        }
                    }
                    $routes[$name] = $name;

                }//if(!in_array($name,$allNav))

            }//foreach (Route::getRoutes() as $route)
        }


        lForm()->setTitle("Menu Create");
        lForm()->pushBreadCrumb(route("admin"), "Admin");
        lForm()->pushBreadCrumb(route("admin.menus"), "Menu");
        return view("admin::livewire.menus.create", compact( "parents", "sorts", "routes", "permissions"))
            ->layout('admin::layouts.master', ['title' => 'Menu Create']);
    }

}
