<?php

namespace Modules\Admin\Http\Livewire\Menus;

use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Illuminate\Support\Arr;
use Livewire\Component;

class Edit extends Component
{
    use WithLaravelFormTrait;
    public $module,$item;
    public $label,$icon,$route,$permission,$sort;
    protected $queryString =["item"];

    protected $rules = [
        "permission"=>"string|required"
        ,"label"=>"string|required"
    ];

    public function mount(){
        $this->onlyLocalhost();
        $navbars = config("admin.menu",[]);
        $data = data_get($navbars,$this->item,[]);
        if(empty($data)){
           return redirect(route('admin.menus'));
        }
        $this->label = data_get($data,"label");
        $this->route = data_get($data,"route");
        $this->icon = data_get($data,"icon");
        $this->permission = data_get($data,"permission");

    }

    public function store(){
        $this->onlyLocalhost();
        $this->validate();
        $navbars = config("admin.menu",[]);
        $data = data_get($navbars,$this->item,[]);
        $data["route"] = $this->route;
        $data["icon"] = $this->icon;
        $data["permission"] = $this->permission;
        $data["label"] = $this->label;
        Arr::set($navbars,$this->item,$data);
        $this->saveNavbar('admin',$navbars);
        session()->flash('message','done');
       return $this->redirect(route("admin.menus"));
    }

    public function render(){

        lForm()->setTitle("Menu Edit");
        lForm()->pushBreadCrumb(route("admin"),"Admin");
        lForm()->pushBreadCrumb(route("admin.menus"), "Menu");
        return view("admin::livewire.menus.edit")
            ->layout('admin::layouts.master', ['title' => 'Menu Edit']);
    }

}
