<?php

namespace Modules\Admin\Http\Livewire\Permissions;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
        lForm()->setTitle("Permissions");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
        lForm()->pushBreadcrumb(route("admin.permissions"),"Permissions");

        return view("admin::livewire.permissions.index")
            ->layout('admin::layouts.master', ['title' => 'Permissions ']);
    }
}
