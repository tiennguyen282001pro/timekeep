<?php

namespace Modules\Admin\Http\Livewire\Permissions;

use App\Models\Permission;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class Listing extends Component
{
    use WithLaravelFormTrait;

    public $confirm = 0;
    public function mount()
    {
        $this->authorize("admin.permissions.listing");
    }

    function delete()
    {
        $this->authorize("admin.permissions.delete");
        if ($this->confirm > 0) {
            Permission::destroy($this->confirm);
        }
        $this->confirm = 0;
        $this->dispatchBrowserEvent('warning', 'Permissions successfully destroyed.');
    }

    public function render()
    {
        $data = Permission::with("children")->whereParentId(0)->whereType("page")->get();
        lForm()->setTitle("Permissions");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.permissions"),"Permissions");
		lForm()->pushBreadcrumb(route("admin.permissions.listing"),"Listing");

        return view("admin::livewire.permissions.listing", compact("data"))
            ->layout('admin::layouts.master', ['title' => 'Permissions Listing']);
    }
}
