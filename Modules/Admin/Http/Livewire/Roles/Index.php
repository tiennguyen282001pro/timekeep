<?php

namespace Modules\Admin\Http\Livewire\Roles;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Roles");
       lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.roles"),"Roles");


        return view("admin::livewire.roles.index")
            ->layout('admin::layouts.master', ['title' => 'Roles']);
    }
}
