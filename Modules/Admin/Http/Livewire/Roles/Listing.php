<?php

namespace Modules\Admin\Http\Livewire\Roles;

use App\Models\Role;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class Listing extends Component
{
    use WithLaravelFormTrait;

    public $confirm = 0;
    // Filter
    public $fId;
    // Sort
    public $sId = 0;
    public $fields = [
        "id" => ["status" => true, "label" => "Id"],
		"name" => ["status" => true, "label" => "Name"],
		"label" => ["status" => true, "label" => "Label"],
		"permissions" => ["status" => true, "label" => "Permissions"],
		"created_at" => ["status" => true, "label" => "Created At"],
		"updated_at" => ["status" => true, "label" => "Updated At"],

    ];

    public function mount()
    {
        $this->authorize("admin.roles.listing");
    }

    function delete()
    {
        $this->authorize("admin.roles.delete");
        if ($this->confirm > 0) {
            Role::destroy($this->confirm);
        }
        $this->confirm = 0;
        $this->dispatchBrowserEvent('warning', 'Roles successfully destroyed.');
    }

    public function render()
    {
        $data = new Role();

        if ($this->fId > 0) {
            $data = $data->whereId($this->fId);
        }
        if ($this->sId == 1) {
            $data = $data->orderBy("id");
        }
        if ($this->sId == 2) {
            $data = $data->orderByDesc("id");
        }
        $data = $data->with([
            "permissions"=>function($qr){
            $qr = $qr->with("parent");
            return $qr;
            }
        ])->paginate(30);
        lForm()->setTitle("Roles Listing");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.roles"),"Roles");
		lForm()->pushBreadcrumb(route("admin.roles.listing"),"Listing");
        return view("admin::livewire.roles.listing", compact("data"))
            ->layout('admin::layouts.master');
    }
}
