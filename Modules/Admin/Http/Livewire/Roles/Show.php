<?php

namespace Modules\Admin\Http\Livewire\Roles;

use App\Models\Role;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("admin.roles.show");
        Role::findOrFail($this->record_id);
    }

    public function render()
    {
        $data =  Role::with([
            "permissions"=>function($qr){
            $qr = $qr->with("parent");
            return $qr;
            }
        ])->findOrFail($this->record_id);
        lForm()->setTitle("Roles Show");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.roles"),"Roles");
		lForm()->pushBreadcrumb(route("admin.roles.show",$this->record_id),"Show");

        return view("admin::livewire.roles.show", compact("data"))
            ->layout('admin::layouts.master');
    }
}
