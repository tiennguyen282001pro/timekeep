<?php

namespace Modules\Admin\Http\Livewire\Emodules;

use App\Models\Emodule;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("admin.emodules.show");
    }

    public function render()
    {
        $data =  Emodule::findOrFail($this->record_id);
        lForm()->setTitle("Emodules Show");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.emodules"),"Emodules");
		lForm()->pushBreadcrumb(route("admin.emodules.show",$this->record_id),"Show");

        return view("admin::livewire.emodules.show", compact("data"))
            ->layout('admin::layouts.master');
    }
}
