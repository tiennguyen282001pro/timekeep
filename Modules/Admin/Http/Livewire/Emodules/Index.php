<?php

namespace Modules\Admin\Http\Livewire\Emodules;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Emodules");
       lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.emodules"),"Emodules");
		

        return view("admin::livewire.emodules.index")
            ->layout('admin::layouts.master', ['title' => 'Emodules']);
    }
}
