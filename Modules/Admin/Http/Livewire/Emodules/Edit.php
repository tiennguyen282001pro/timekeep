<?php

namespace Modules\Admin\Http\Livewire\Emodules;

use App\Models\Emodule;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Edit extends Component
{
    use WithLaravelFormTrait;

    public $name, $label, $slug, $icon, $color, $permission, $parent_id, $active, $type;

    protected function rules()
    {
        return [
            'name' => '',
			'label' => '',
			'slug' => '',
			'icon' => '',
			'color' => '',
			'permission' => '',
			'parent_id' => '',
			'active' => '',
			'type' => '',

        ];
    }

    public function mount()
    {
        $this->authorize("admin.emodules.edit");
        $data = Emodule::findOrFail($this->record_id);
        $this->name = $data->name;
		$this->label = $data->label;
		$this->slug = $data->slug;
		$this->icon = $data->icon;
		$this->color = $data->color;
		$this->permission = $data->permission;
		$this->parent_id = $data->parent_id;
		$this->active = $data->active;
		$this->type = $data->type;

    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("admin.emodules.edit");
        $this->validate();
        $data = Emodule::findOrFail($this->record_id);
        $data->fill([
            'name' => $this->name,
			'label' => $this->label,
			'slug' => $this->slug,
			'icon' => $this->icon,
			'color' => $this->color,
			'permission' => $this->permission,
			'parent_id' => $this->parent_id,
			'active' => $this->active,
			'type' => $this->type,

        ]);
        if (!$data->clean) {
            $data->update();
            $this->redirectForm("admin.emodules", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Emodules Edit");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.emodules"),"Emodules");
		lForm()->pushBreadcrumb(route("admin.emodules.edit",$this->record_id),"Edit");
        $emodule = Emodule::where('parent_id',0)->where('active',1)->get()->pluck('name','id');
        return view("admin::livewire.emodules.edit",compact('emodule'))
            ->layout('admin::layouts.master');
    }
}
