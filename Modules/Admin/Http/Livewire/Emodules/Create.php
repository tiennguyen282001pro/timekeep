<?php

namespace Modules\Admin\Http\Livewire\Emodules;

use App\Models\Emodule;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class   Create extends Component
{
    use WithLaravelFormTrait;

    public $name, $label, $slug, $icon, $color, $permission, $parent_id, $active, $type;
    protected $rules = [
        'name' => 'required',
		'label' => '',
		'slug' => '',
		'icon' => '',
		'color' => '',
		'permission' => '',
		'parent_id' => '',
		'active' => '',
		'type' => '',

    ];
    protected $messages = [
       'name.required'=> 'Trường này không được bỏ trống',
    ];

    public function mount()
    {
        $this->authorize("admin.emodules.create");
        $this->done = 1;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("admin.emodules.create");
        $this->validate();
        $data = Emodule::create([
            'name' => $this->name,
			'label' => $this->label,
			'slug' => $this->slug,
			'icon' => $this->icon,
			'color' => $this->color,
			'permission' => $this->permission,
			'parent_id' => $this->parent_id,
			'active' => $this->active,
			'type' => $this->type,

        ]);
        if ($data) {
            $this->redirectForm("admin.emodules", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Emodules Create");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.emodules"),"Emodules");
		lForm()->pushBreadcrumb(route("admin.emodules.create"),"Create");

        $emodule = Emodule::where('parent_id',0)->where('active',1)->get()->pluck('name','id');
        return view("admin::livewire.emodules.create",compact('emodule'))
            ->layout('admin::layouts.master');
    }
}
