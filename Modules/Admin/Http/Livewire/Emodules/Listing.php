<?php

namespace Modules\Admin\Http\Livewire\Emodules;

use App\Models\Emodule;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\WithPagination;
use Livewire\Component;

class Listing extends Component
{
    use WithLaravelFormTrait;
     use  WithPagination;

    public $confirm = 0;
    // Filter
    public $fId;
    // Sort
    public $sId = 0;
    public $fields = [
        "id" => ["status" => true, "label" => "Id"],
		"name" => ["status" => true, "label" => "Name"],
		"label" => ["status" => true, "label" => "Label"],
		"slug" => ["status" => true, "label" => "Slug"],
		"icon" => ["status" => true, "label" => "Icon"],
		"color" => ["status" => true, "label" => "Color"],
		"permission" => ["status" => true, "label" => "Permission"],
		"parent_id" => ["status" => true, "label" => "Parent Id"],
		"active" => ["status" => true, "label" => "Active"],
		"type" => ["status" => true, "label" => "Type"],
		"created_at" => ["status" => true, "label" => "Created At"],
		"updated_at" => ["status" => true, "label" => "Updated At"],

    ];

    public function changeActive($record_id)
    {
        $data = Emodule::findOrFail($record_id);
        $data->update([
            'active' => !$data->active,
        ]);

    }

    public function mount()
    {
        $this->authorize("admin.emodules.listing");
    }

    public function delete()
    {
        $this->authorize("admin.emodules.delete");
        if ($this->confirm > 0) {
            Emodule::destroy($this->confirm);
        }
        $this->confirm = 0;
        $this->dispatchBrowserEvent('warning', 'Emodules successfully destroyed.');
    }

    public function render()
    {
        $data = new Emodule();

        if ($this->fId > 0) {
            $data = $data->whereId($this->fId);
        }
        if ($this->sId == 1) {
            $data = $data->orderBy("id");
        }
        if ($this->sId == 2) {
            $data = $data->orderByDesc("id");
        }
        $data = $data->paginate(30);

        lForm()->setTitle("Emodules Listing");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.emodules"),"Emodules");
		lForm()->pushBreadcrumb(route("admin.emodules.listing"),"Listing");
        return view("admin::livewire.emodules.listing", compact("data"))
            ->layout('admin::layouts.master');
    }
}
