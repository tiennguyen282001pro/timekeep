<?php

namespace Modules\Admin\Http\Livewire\Users;

use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("admin.users.show");
        User::findOrFail($this->record_id);
    }

    public function render()
    {
        $data =  User::findOrFail($this->record_id);
        lForm()->setTitle("Users");
        lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.users"),"Users");
		lForm()->pushBreadcrumb(route("admin.users.show",$this->record_id),"Show");

        return view("admin::livewire.users.show", compact("data"))
            ->layout('admin::layouts.master', ['title' => 'Users Show']);
    }
}
