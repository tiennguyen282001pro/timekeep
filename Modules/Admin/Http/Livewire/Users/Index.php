<?php

namespace Modules\Admin\Http\Livewire\Users;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Users");
       lForm()->pushBreadcrumb(route("admin"),"Admin");
		lForm()->pushBreadcrumb(route("admin.users"),"Users");


        return view("admin::livewire.users.index")
            ->layout('admin::layouts.master', ['title' => 'Users']);
    }
}
