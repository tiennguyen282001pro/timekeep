<?php
Route::get('/', [\Modules\Admin\Http\Controllers\HomeController::class, "index"]);
Route::get("/icons", \Modules\Admin\Http\Livewire\Icons\Listing::class)->name('.icons');
Route::get("/menus", \Modules\Admin\Http\Livewire\Menus\Listing::class)->name('.menus');
Route::get("/permission-configs", \Modules\Admin\Http\Livewire\PermissionConfigs\Listing::class)->name('.permission-configs');

Route::prefix("admins")->name(".admins")->group(function (){
    Route::get("/", \Modules\Admin\Http\Livewire\Admins\Index::class)->can("admin.admins");
    Route::get("/listing", \Modules\Admin\Http\Livewire\Admins\Listing::class)->name(".listing")->can("admin.admins.listing");
    Route::get("/create", \Modules\Admin\Http\Livewire\Admins\Create::class)->name(".create")->can("admin.admins.create");
    Route::get("/edit/{record_id}", \Modules\Admin\Http\Livewire\Admins\Edit::class)->name(".edit")->can("admin.admins.edit");
    Route::get("/show/{record_id}", \Modules\Admin\Http\Livewire\Admins\Show::class)->name(".show")->can("admin.admins.show");
    //---END-OF-ROLES---//
});

Route::prefix("users")->name(".users")->group(function (){
    Route::get("/", \Modules\Admin\Http\Livewire\Users\Index::class)->can("admin.users");
    Route::get("/listing", \Modules\Admin\Http\Livewire\Users\Listing::class)->name(".listing")->can("admin.users.listing");
    Route::get("/create", \Modules\Admin\Http\Livewire\Users\Create::class)->name(".create")->can("admin.users.create");
    Route::get("/edit/{record_id}", \Modules\Admin\Http\Livewire\Users\Edit::class)->name(".edit")->can("admin.users.edit");
    Route::get("/show/{record_id}", \Modules\Admin\Http\Livewire\Users\Show::class)->name(".show")->can("admin.users.show");
    //---END-OF-ROLES---//
});

Route::prefix("roles")->name(".roles")->group(function (){
    Route::get("/", \Modules\Admin\Http\Livewire\Roles\Index::class)->can("admin.roles");
    Route::get("/listing", \Modules\Admin\Http\Livewire\Roles\Listing::class)->name(".listing")->can("admin.roles.listing");
    Route::get("/create", \Modules\Admin\Http\Livewire\Roles\Create::class)->name(".create")->can("admin.roles.create");
    Route::get("/edit/{record_id}", \Modules\Admin\Http\Livewire\Roles\Edit::class)->name(".edit")->can("admin.roles.edit");
    Route::get("/show/{record_id}", \Modules\Admin\Http\Livewire\Roles\Show::class)->name(".show")->can("admin.roles.show");
    //---END-OF-ROLES---//
});

Route::prefix("permissions")->name(".permissions")->group(function (){
    Route::get("/", \Modules\Admin\Http\Livewire\Permissions\Index::class)->can("admin.permissions");
    Route::get("/listing", \Modules\Admin\Http\Livewire\Permissions\Listing::class)->name(".listing")->can("admin.permissions.listing");
    Route::get("/create", \Modules\Admin\Http\Livewire\Permissions\Create::class)->name(".create")->can("admin.permissions.create");
    Route::get("/edit/{record_id}", \Modules\Admin\Http\Livewire\Permissions\Edit::class)->name(".edit")->can("admin.permissions.edit");
    Route::get("/show/{record_id}", \Modules\Admin\Http\Livewire\Permissions\Show::class)->name(".show")->can("admin.permissions.show");
    //---END-OF-PERMISSIONS---//
});

Route::prefix("emodules")->name(".emodules")->group(function (){
    Route::get("/", \Modules\Admin\Http\Livewire\Emodules\Index::class)->can("admin.emodules");
    Route::get("/listing", \Modules\Admin\Http\Livewire\Emodules\Listing::class)->name(".listing")->can("admin.emodules.listing");
    Route::get("/create", \Modules\Admin\Http\Livewire\Emodules\Create::class)->name(".create")->can("admin.emodules.create");
    Route::get("/edit/{record_id}", \Modules\Admin\Http\Livewire\Emodules\Edit::class)->name(".edit")->can("admin.emodules.edit");
    Route::get("/show/{record_id}", \Modules\Admin\Http\Livewire\Emodules\Show::class)->name(".show")->can("admin.emodules.show");
    //---END-OF-EMODULES---//
});

Route::prefix("notifies")->name(".notifies")->group(function (){
    Route::get("/", \Modules\Admin\Http\Livewire\Notifies\Index::class)->can("admin.notifies");
    Route::get("/listing", \Modules\Admin\Http\Livewire\Notifies\Listing::class)->name(".listing")->can("admin.notifies.listing");
    Route::get("/create", \Modules\Admin\Http\Livewire\Notifies\Create::class)->name(".create")->can("admin.notifies.create");
    Route::get("/edit/{record_id}", \Modules\Admin\Http\Livewire\Notifies\Edit::class)->name(".edit")->can("admin.notifies.edit");
    Route::get("/show/{record_id}", \Modules\Admin\Http\Livewire\Notifies\Show::class)->name(".show")->can("admin.notifies.show");
    //---END-OF-NOTIFIES---//
});

//---END-OF-ROUTES---//
