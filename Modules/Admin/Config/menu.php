<?php
 return [
	'menu' => [
 		[
			'label' => 'Admins User',
			'icon' => 'account-manager',
			'route' => 'admin.admins',
			'permission' => 'admin.admins',
			'children' => [
				[
					'label' => 'Listing',
					'icon' => 'list',
					'route' => 'admin.admins.listing',
					'permission' => 'admin.admins.lsiting',
				],
				[
					'label' => 'Create',
					'icon' => 'add',
					'route' => 'admin.admins.create',
					'permission' => 'admin.admins.create',
				],
			],
		],
		[
			'label' => 'Users',
			'icon' => 'people',
			'route' => 'admin.users',
			'permission' => 'admin.users',
			'children' => [
				[
					'label' => 'Listing',
					'icon' => 'list',
					'route' => 'admin.users.listing',
					'permission' => 'admin.users.listing',
				],
				[
					'label' => 'Create',
					'icon' => 'add',
					'route' => 'admin.users.create',
					'permission' => 'admin.users.create',
				],
			],
		],
		[
			'label' => 'Roles',
			'icon' => 'verified',
			'route' => 'admin.roles',
			'permission' => 'admin.roles',
			'children' => [
				[
					'label' => 'Listing',
					'icon' => 'list',
					'route' => 'admin.roles.listing',
					'permission' => 'admin.roles.listing',
				],
				[
					'label' => 'Create',
					'icon' => 'add',
					'route' => 'admin.roles.create',
					'permission' => 'admin.roles.create',
				],
			],
		],
		[
			'label' => 'Permissions',
			'icon' => 'security',
			'route' => 'admin.permissions',
			'permission' => 'admin.permissions',
			'children' => [
				[
					'label' => 'Listing',
					'icon' => 'list',
					'route' => 'admin.permissions.listing',
					'permission' => 'admin.permissions.listing',
				],
				[
					'label' => '
					',
					'icon' => 'add',
					'route' => 'admin.permissions.create',
					'permission' => 'admin.permissions.create',
				],
			],
		],

	]
];
