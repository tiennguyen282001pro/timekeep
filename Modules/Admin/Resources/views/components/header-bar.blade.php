<header>
    <label for="lf-control" class="h-btn h-menu">{!! lfIcon("menu") !!}</label>
    <div class="header-bar"></div>
    {{--begin input search--}}
    <div class="bar-search mr-6"  >
        <div class="h-full">
            <div class="input_search h-full flex items-center justify-center relative " >
                <input type="text" name="search_btn"  placeholder="Tìm kiếm ..." class="px-9 outline-0 w-80 border-0 rounded-md bg-red-50"  >

                <div class="edit_icon absolute left-2 top-4  " style="color:#484848a8;">
                    <label for="" class=""> {!! lfIcon('search',22) !!}</label>
                </div>
                <div class="icon_tune  absolute right-2 text-slate-400 hover:text-black " style="transition: 1.5s">
                    <label for="">{!! lfIcon('tune',22) !!}</label>
                </div>
            </div>
        </div>
    </div>
    {{--end input search--}}

    {{--begin form emodule--}}
    <div class="module_show w-12 h-12 relative " x-data="{ open: false }" >
        <div class="open h-full w-full hover:bg-red-600"   @click="open=!open"    :class="open ? 'bg-red-600' : ''">
            <label class="loading icon h-full w-full flex cursor-pointer items-center justify-center text-white" >
                {!! lfIcon('app') !!}
            </label>
        </div>
        <div class="box_right absolute right-0 bg-white shadow-2xl "   x-show="open" x-transition.duration.500ms  @click.away="open = false" style="display: none"  >
            <div class="mounse">
            </div>
            <div class="flex">
                @foreach($ruslt as $key => $item)
                    <div class="w-full py-5  border-r">
                        <h4 class="uppercase font-semibold text-lg ml-11 mt-5 mb-5">
                            @if(!empty($item['title']))
                                {{$item['title']}}
                            @endif
                        </h4>
                        <div class="grid  grid-cols-2 px-4  w-max ">
                            @if(!empty($item['child']))
                                @foreach($item['child'] as $key => $value)
                                    <a href="@if(empty($value->slug))#@else{{ route("$value->slug")}}@endif" class="app-menu-caption inline-flex flex-col text-center items-center  p-4 mb-2 hover:bg-zinc-100 hover:rounded-md" >

                                        <div class="p-2.5 text-sky-50 rounded-md "  style="
                                        background-color:
                                        @if($value->color)
                                        {{$value->color}}
                                        @else
                                        #0ea5e9
                                        @endif
                                        ">
                                            {!! lfIcon($value->icon,25) !!}
                                        </div>
                                        <span class="mt-3 text-sm font-normal" style="max-width: 70px;">{{$value->name}}</span>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- end emodule -->
     <x-lf.btn.dropdown class="h-user" icon="person">
        <div class="user">
            <span class="icon">{!! lfIcon('person',56) !!}</span>
            <span class="name">{{Auth::user()->name}}</span>
        </div>
        <div class="action">
            <a class="btn">Setting</a>
            <form method="post" action="{{route('logout')}}">
                @csrf
                <button class="btn">Logout</button>
            </form>
        </div>
    </x-lf.btn.dropdown>
</header>
