<div class="w-full flex flex-wrap p-2">
    @can("admin.users.listing")
        <a href="{{route("admin.users.listing")}}" title="Users Listing" class="btn-primary xl m-2">
            {!! lfIcon("list",32) !!}
            <span class="text">Listing</span>
        </a>
    @endcan
    @can("admin.users.create")
        <a href="{{route("admin.users.create")}}" title="Users Listing" class="btn-success xl m-2">
            {!! lfIcon("add",32) !!}
            <span class="text">Create</span>
        </a>
    @endcan
</div>
