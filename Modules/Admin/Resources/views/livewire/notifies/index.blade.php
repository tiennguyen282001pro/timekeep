<div class="w-full flex flex-wrap p-2">
    @can("admin.notifies.listing")
        <a href="{{route("admin.notifies.listing")}}" title="Notifies Listing" class="btn-primary xl m-2">
            {!! lfIcon("list",32) !!}
            <span class="text">Listing</span>
        </a>
    @endcan
    @can("admin.notifies.create")
        <a href="{{route("admin.notifies.create")}}" title="Notifies Listing" class="btn-success xl m-2">
            {!! lfIcon("add",32) !!}
            <span class="text">Create</span>
        </a>
    @endcan
</div>
