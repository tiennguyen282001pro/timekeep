<x-lf.page.listing :fields="$fields" :footer="$data->onEachSide(3)->links()">
    <table class="table">
        <thead>
        <tr>
            <th><x-lf.form.sort name="sId" :value="$sId">#</x-lf.form.sort></th>
            <x-lf.table.label name="recipient" :fields="$fields">Người nhận</x-lf.table.label>
			<x-lf.table.label name="sender" :fields="$fields">Người gửi</x-lf.table.label>
			<x-lf.table.label name="content" :fields="$fields">Nội dụng</x-lf.table.label>
{{--			<x-lf.table.label name="type" :fields="$fields">Type</x-lf.table.label>--}}
			<x-lf.table.label name="status" :fields="$fields">Trạng thái</x-lf.table.label>
			<x-lf.table.label name="created_at" :fields="$fields">Thời gian gửi</x-lf.table.label>
{{--			<x-lf.table.label name="updated_at" :fields="$fields">Updated At</x-lf.table.label>--}}

            <th></th>
        </tr>
        </thead>
        @foreach($data as $item)
            <tr>
                <th class="stt">{{$item->id}}</th>
                <x-lf.table.item name="recipient" :fields="$fields">{{$item->user->name}}</x-lf.table.item>
				<x-lf.table.item name="sender" :fields="$fields">{{$item->senders->name}}</x-lf.table.item>
				<x-lf.table.item name="content" :fields="$fields">{{$item->content}}</x-lf.table.item>
{{--				<x-lf.table.item name="type" :fields="$fields">{{$item->type}}</x-lf.table.item>--}}
				<x-lf.table.item name="status" :fields="$fields">
                    <x-lf.btn.toggle :val="$item->status" wire:change="changeStatus({{$item->id}})" />
                </x-lf.table.item>
				<x-lf.table.item name="created_at" :fields="$fields">{{$item->created_at->format('H:s:i')}}</x-lf.table.item>
{{--				<x-lf.table.item name="updated_at" :fields="$fields">{{$item->updated_at}}</x-lf.table.item>--}}

                <td class="action">
                    @can('admin.notifies.show')
                    <a class="btn-success xs" href="{{route("admin.notifies.show",$item->id)}}">{!! lfIcon("launch",10) !!}</a>
                    @endcan
                    @can('admin.notifies.edit')
                    <a class="btn-info xs" href="{{route("admin.notifies.edit",$item->id)}}">{!! lfIcon("edit",10) !!}</a>
                    @endcan
                    @can('admin.notifies.delete')
                    <x-lf.btn.delete :record="$item->id" :confirm="$confirm"/>
                    @endcan
                </td>
            </tr>
        @endforeach
    </table>
    <x-slot:filters>
        <x-lf.filter.input name="fId" type="number" placeholder="Id ..." />
    </x-slot:filters>
    <x-slot:tools>
        @can("admin.notifies.create")
           <div> <a class="btn-primary sm" href="{{route("admin.notifies.create")}}">{!! lfIcon("add") !!}</a></div>
        @endcan
    </x-slot:tools>
</x-lf.page.listing>
