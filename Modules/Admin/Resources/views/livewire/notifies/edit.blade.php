<div class="w-full p-1 md:p-4">
    <x-lf.card title="Sửa" class="warning">
        <x-lf.form.select name="recipient" :default="[0 => '--- Chọn người nhận ---']" type="integer"  placeholder="Recipient ...">
            <x-slot:label>
                <span>Người nhận</span> <label for="" class="text-red-600">(*)</label>
            </x-slot:label>
        </x-lf.form.select>
        <x-lf.form.select name="sender" type="integer" :default="[0 => '--- Chọn người gửi ---']" label="Sender" placeholder="Sender ..." >
            <x-slot:label>
                <span>Người gửi</span> <label for="" class="text-red-600">(*)</label>
            </x-slot:label>
        </x-lf.form.select>
        <x-lf.form.textarea name="content" type="string" label="Nội dung" placeholder="Nhập nội dung ..." />
        {{--		<x-lf.form.toggle name="type" label="Type" />--}}
        <x-lf.form.toggle name="status" label="Trạng thái" />

        <x-lf.form.done :params="['Listing','Create','Show']"/>
        <x-slot:tools >
            @can('admin.notifies.show')
            <a class="btn-success sm" href="{{route('admin.notifies.show',$record_id)}}">{!! lfIcon("launch",11) !!}</a>
            @endcan
            <a class="btn-primary sm" href="{{route('admin.notifies')}}">{!! lfIcon("list",11) !!}</a>
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                <label class="btn-primary flex-none" wire:click="store">Update</label>
                <a class="btn" href="{{route("admin.notifies")}}">Cancel</a>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>

