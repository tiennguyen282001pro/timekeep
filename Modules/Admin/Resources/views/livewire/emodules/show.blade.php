<div class="w-full p-2 md:p-4 max-w-lg">
    <x-lf.card class="success" title="Show">
        <table class="table">
            <tr>
                <th class="text-right pr-2">ID:</th>
                <td>{{$data->id}}</td>
            </tr>
            
			<tr>
				<th class="text-right pr-2">Name:</th>
				<td>{{$data->name}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Label:</th>
				<td>{{$data->label}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Slug:</th>
				<td>{{$data->slug}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Icon:</th>
				<td>{{$data->icon}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Color:</th>
				<td>{{$data->color}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Permission:</th>
				<td>{{$data->permission}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Parent Id:</th>
				<td>{{$data->parent_id}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Active:</th>
				<td>{{$data->active}}</td>
			</tr>
			<tr>
				<th class="text-right pr-2">Type:</th>
				<td>{{$data->type}}</td>
			</tr>
        </table>
        <x-slot:tools>
            @can("admin.emodules.listing")
                <a class="btn-primary xs" href="{{route("admin.emodules")}}">{!! lfIcon("list",11) !!}</a>
            @endcan
            @can("admin.emodules.edit")
                <a class="btn-warning xs" href="{{route("admin.emodules.edit",$data->id)}}">{!! lfIcon("edit",11) !!}</a>
            @endcan
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                @can("admin.emodules.listing")
                    <a class="btn-primary" href="{{route("admin.emodules")}}">{!! lfIcon("list") !!} <span>Listing</span></a>
                @endcan
                <div>
                    @can("admin.emodules.edit")
                        <a class="btn-warning" href="{{route("admin.emodules.edit",$data->id)}}">{!! lfIcon("edit") !!} <span>Edit</span></a>
                    @endcan
                </div>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>
