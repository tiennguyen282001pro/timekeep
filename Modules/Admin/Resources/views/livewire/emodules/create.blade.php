<div class="w-full p-1 md:p-4">
    <x-lf.card title="Thêm mới" class="info w-1/2" >
        <x-lf.form.input name="name" type="string"  oninput="this.value = this.value.charAt(0).toUpperCase() + this.value.slice(1)" placeholder="Nhập tên module ...">
            <x-slot:label>
                <span >Tên module <span class="text-red-500">(*)</span></span>
            </x-slot:label>
        </x-lf.form.input>
{{--        <x-lf.form.input name="label" type="string" class="w-1/2" label="Label" placeholder="Label ..."/>--}}
        <x-lf.form.input name="slug" type="string"  class="w-full" label="Router" placeholder="Nhập router ..."/>
        <x-lf.form.select name="parent_id" class="w-full " type="integer" :default="[0=> '--- Cấp cha ---']" :params="$emodule" label="Cấp cha" placeholder="Parent id ..."/>
        <x-lf.form.icon name="icon"  label="Icon" :val="$icon" class="w-full md:w-1/2"/>
        <x-lf.form.input  class="w-full md:w-1/2" style="width: 20%"  name="color" type="color"   label="Màu sắc" placeholder="Color ..."/>
{{--		<x-lf.form.input name="permission" type="string" label="Permission" placeholder="Permission ..."/>--}}
		<x-lf.form.toggle name="active" label="Trạng thái" />
{{--		<x-lf.form.input name="type" type="integer" label="Type" placeholder="Type ..."/>--}}

        <x-lf.form.done />
        <x-slot:tools>
            <a class="btn-primary sm" href="{{route('admin.emodules')}}">{!! lfIcon("list") !!}</a>
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                <label class="btn-primary flex-none" wire:click="store">Create</label>
                <a class="btn" href="{{route("admin.emodules")}}">Cancel</a>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>



