<div class="w-full flex flex-wrap p-2">
    @can("admin.emodules.listing")
        <a href="{{route("admin.emodules.listing")}}" title="Emodules Listing" class="btn-primary xl m-2">
            {!! lfIcon("list",32) !!}
            <span class="text">Listing</span>
        </a>
    @endcan
    @can("admin.emodules.create")
        <a href="{{route("admin.emodules.create")}}" title="Emodules Listing" class="btn-success xl m-2">
            {!! lfIcon("add",32) !!}
            <span class="text">Create</span>
        </a>
    @endcan
</div>
