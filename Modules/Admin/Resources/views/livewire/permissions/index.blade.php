<div class="w-full flex flex-wrap p-2">
    @can("admin.permissions.listing")
        <a href="{{route("admin.permissions.listing")}}" title="Permission Listing" class="btn-primary xl m-2">
            {!! lfIcon("list",32) !!}
            <span class="text">Listing</span>
        </a>
    @endcan
    @can("admin.permissions.create")
        <a href="{{route("admin.permissions.create")}}" title="Permission Listing" class="btn-success xl m-2">
            {!! lfIcon("add",32) !!}
            <span class="text">Create</span>
        </a>
    @endcan
</div>
