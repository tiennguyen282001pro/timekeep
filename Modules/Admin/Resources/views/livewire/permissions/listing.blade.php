<div class="w-full p-2 md:p-4">
    <x-lf.card title="Listing">
        <div class="tree">
            <div class="tree-content">
                @foreach($data as $item)
                    <div class="item">
                        <div class="item-content">
                            <div class="flex-1">
                                <span>{{$item->label}}</span>
                                <span class="text-red-700">({{$item->name}})</span>
                                <span>{{$item->type}}</span>
                            </div>
                            <div class="tools">
                                @can("admin.permissions.show")
                                    <a href="{{route("admin.permissions.show",$item->id)}}" class="btn-info xs">{!! lfIcon("launch",11) !!}</a>
                                @endcan
                                @can("admin.permissions.edit")
                                    <a href="{{route("admin.permissions.edit",$item->id)}}" class="btn-warning xs">{!! lfIcon("edit",11) !!}</a>
                                @endcan
                                @can("admin.permissions.delete")
                                    <x-lf.btn.delete :record="$item->id" :confirm="$confirm" />
                                @endcan
                            </div>
                        </div>
                        @if($item->children)
                            <div class="tree-content">
                                @foreach($item->children as $child)
                                    <div class="item">
                                        <div class="item-content">
                                            <div class="flex-1">
                                                <span>{{$child->label}}</span>
                                                <span class="text-red-700">({{$child->name}})</span>
                                                <span>{{$child->type}}</span>
                                            </div>
                                            <div class="tools">
                                                @can("admin.permissions.show")
                                                    <a href="{{route("admin.permissions.show",$child->id)}}" class="btn-info xs">{!! lfIcon("launch",11) !!}</a>
                                                @endcan
                                                @can("admin.permissions.edit")
                                                    <a href="{{route("admin.permissions.edit",$child->id)}}" class="btn-warning xs">{!! lfIcon("edit",11) !!}</a>
                                                @endcan
                                                @can("admin.permissions.delete")
                                                    <x-lf.btn.delete :record="$child->id" :confirm="$confirm" />
                                                @endcan
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
        <x-slot:tools>
                    @can("admin.permissions.create")
                        <div> <a class="btn-primary sm" href="{{route("admin.permissions.create")}}">{!! lfIcon("add") !!}</a></div>
                    @endcan
                </x-slot:tools>
    </x-lf.card>
</div>
