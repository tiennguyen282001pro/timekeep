<?php

namespace Modules\Admin\Views\Components;



use App\Models\Emodule;
use Illuminate\View\Component;

class HeaderBar extends Component
{

    public function render(){
        $emodule  = Emodule::where("active",1)->get();
        $ruslt = [];

        foreach ($emodule as $key => $value)
        {
            if ($value->parent_id == 0)
            {
                $ruslt[$value->id]["title"] = $value->name;
                $id[] = $value["id"];
            }
            if ($value->parent_id != 0)
            {
                foreach ($id as $val)
                {
                    if ($value->parent_id == $val)
                    {
                        $ruslt[$val]["child"][] = $value;
                    }
                }
            }

        }

        return view('admin::components.header-bar',compact('ruslt'));
    }
}
