<div class="w-full p-1 md:p-4">
    <x-lf.card title="Create" class="info">
        <x-lf.form.input name="name" type="string" label="Name" placeholder="Name ..."/>
		<x-lf.form.input name="email" type="string" label="Email" placeholder="Email ..."/>
		<x-lf.form.input name="email_verified_at" type="datetime" label="Email verified at" placeholder="Email verified at ..."/>
		<x-lf.form.input name="password" type="string" label="Password" placeholder="Password ..."/>
		<x-lf.form.input name="two_factor_confirmed_at" type="datetime" label="Two factor confirmed at" placeholder="Two factor confirmed at ..."/>
		<x-lf.form.input name="current_team_id" type="bigint" label="Current team id" placeholder="Current team id ..."/>
		<x-lf.form.input name="profile_photo_path" type="string" label="Profile photo path" placeholder="Profile photo path ..."/>
		<x-lf.form.toggle name="is_admin" label="Is admin" />
		<x-lf.form.toggle name="is_super_admin" label="Is super admin" />
		
        <x-lf.form.done />
        <x-slot:tools>
            <a class="btn-primary sm" href="{{route('demo.users')}}">{!! lfIcon("list") !!}</a>
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                <label class="btn-primary flex-none" wire:click="store">Create</label>
                <a class="btn" href="{{route("demo.users")}}">Cancel</a>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>



