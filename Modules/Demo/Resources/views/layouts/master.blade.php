@props(['title'=>'Demo'])
<x-erp-layout module="demo" :title="$title">
    <x-slot:assets>

    </x-slot:assets>
    {{$slot}}
</x-erp-layout>
