<?php
Route::get('/', [\Modules\Demo\Http\Controllers\HomeController::class,"index"])->can('demo');

Route::prefix("users")->name(".users")->group(function (){
    Route::get("/", \Modules\Demo\Http\Livewire\Users\Index::class)->can("demo.users");
    Route::get("/listing", \Modules\Demo\Http\Livewire\Users\Listing::class)->name(".listing")->can("demo.users.listing");
    Route::get("/create", \Modules\Demo\Http\Livewire\Users\Create::class)->name(".create")->can("demo.users.create");
    Route::get("/edit/{record_id}", \Modules\Demo\Http\Livewire\Users\Edit::class)->name(".edit")->can("demo.users.edit");
    Route::get("/show/{record_id}", \Modules\Demo\Http\Livewire\Users\Show::class)->name(".show")->can("demo.users.show");
    //---END-OF-USERS---//
});

//---END-OF-ROUTES---//
