<?php

namespace Modules\Demo\Http\Livewire\Users;

use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("demo.users.show");
    }

    public function render()
    {
        $data =  User::findOrFail($this->record_id);
        lForm()->setTitle("Users Show");
        lForm()->pushBreadcrumb(route("demo"),"Demo");
		lForm()->pushBreadcrumb(route("demo.users"),"Users");
		lForm()->pushBreadcrumb(route("demo.users.show",$this->record_id),"Show");

        return view("demo::livewire.users.show", compact("data"))
            ->layout('demo::layouts.master');
    }
}
