<?php

namespace Modules\Demo\Http\Livewire\Users;

use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class   Create extends Component
{
    use WithLaravelFormTrait;

    public $name, $email, $email_verified_at, $password, $two_factor_confirmed_at, $current_team_id, $profile_photo_path, $is_admin, $is_super_admin;
    protected $rules = [
        'name' => 'string',
		'email' => 'email',
		'email_verified_at' => '',
		'password' => 'required|min:8',
		'two_factor_confirmed_at' => '',
		'current_team_id' => '',
		'profile_photo_path' => '',
		'is_admin' => '',
		'is_super_admin' => '',
		
    ];

    public function mount()
    {
        $this->authorize("demo.users.create");
        $this->done = 1;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("demo.users.create");
        $this->validate();
        $data = User::create([
            'name' => $this->name,
			'email' => $this->email,
			'email_verified_at' => $this->email_verified_at,
			'password' => $this->password,
			'two_factor_confirmed_at' => $this->two_factor_confirmed_at,
			'current_team_id' => $this->current_team_id,
			'profile_photo_path' => $this->profile_photo_path,
			'is_admin' => $this->is_admin,
			'is_super_admin' => $this->is_super_admin,
			
        ]);
        if ($data) {
            $this->redirectForm("demo.users", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Users Create");
        lForm()->pushBreadcrumb(route("demo"),"Demo");
		lForm()->pushBreadcrumb(route("demo.users"),"Users");
		lForm()->pushBreadcrumb(route("demo.users.create"),"Create");
		
        return view("demo::livewire.users.create")
            ->layout('demo::layouts.master');
    }
}
