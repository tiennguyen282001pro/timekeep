<?php

namespace Modules\Demo\Http\Livewire\Users;

use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Edit extends Component
{
    use WithLaravelFormTrait;

    public $name, $email, $email_verified_at, $password, $two_factor_confirmed_at, $current_team_id, $profile_photo_path, $is_admin, $is_super_admin;

    protected function rules()
    {
        return [
            'name' => 'string',
			'email' => 'email',
			'email_verified_at' => '',
			'password' => 'required|min:8',
			'two_factor_confirmed_at' => '',
			'current_team_id' => '',
			'profile_photo_path' => '',
			'is_admin' => '',
			'is_super_admin' => '',
			
        ];
    }

    public function mount()
    {
        $this->authorize("demo.users.edit");
        $data = User::findOrFail($this->record_id);
        $this->name = $data->name;
		$this->email = $data->email;
		$this->email_verified_at = $data->email_verified_at;
		$this->password = $data->password;
		$this->two_factor_confirmed_at = $data->two_factor_confirmed_at;
		$this->current_team_id = $data->current_team_id;
		$this->profile_photo_path = $data->profile_photo_path;
		$this->is_admin = $data->is_admin;
		$this->is_super_admin = $data->is_super_admin;
		
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("demo.users.edit");
        $this->validate();
        $data = User::findOrFail($this->record_id);
        $data->fill([
            'name' => $this->name,
			'email' => $this->email,
			'email_verified_at' => $this->email_verified_at,
			'password' => $this->password,
			'two_factor_confirmed_at' => $this->two_factor_confirmed_at,
			'current_team_id' => $this->current_team_id,
			'profile_photo_path' => $this->profile_photo_path,
			'is_admin' => $this->is_admin,
			'is_super_admin' => $this->is_super_admin,
			
        ]);
        if (!$data->clean) {
            $data->update();
            $this->redirectForm("demo.users", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Users Edit");
        lForm()->pushBreadcrumb(route("demo"),"Demo");
		lForm()->pushBreadcrumb(route("demo.users"),"Users");
		lForm()->pushBreadcrumb(route("demo.users.edit",$this->record_id),"Edit");

        return view("demo::livewire.users.edit")
            ->layout('demo::layouts.master');
    }
}
