<?php

namespace Modules\Demo\Http\Livewire\Users;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Users");
       lForm()->pushBreadcrumb(route("demo"),"Demo");
		lForm()->pushBreadcrumb(route("demo.users"),"Users");
		

        return view("demo::livewire.users.index")
            ->layout('demo::layouts.master', ['title' => 'Users']);
    }
}
