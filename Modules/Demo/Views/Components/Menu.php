<?php

namespace Modules\Demo\Views\Components;

use Illuminate\View\Component;

class Menu extends Component
{

    public function render()
    {
        $data = config("demo.menu",[]);

        return view("demo::components.menu", compact("data"));
    }
}
