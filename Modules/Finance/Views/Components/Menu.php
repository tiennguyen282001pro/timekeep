<?php

namespace Modules\Finance\Views\Components;

use Illuminate\View\Component;

class Menu extends Component
{

    public function render()
    {
        $data = config("finance.menu",[]);

        return view("finance::components.menu", compact("data"));
    }
}
