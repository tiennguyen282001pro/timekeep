<?php

namespace Modules\Finance\Views\Components;



use Illuminate\View\Component;

class HeaderBar extends Component
{

    public function render(){

        return view('finance::components.header-bar');
    }
}
