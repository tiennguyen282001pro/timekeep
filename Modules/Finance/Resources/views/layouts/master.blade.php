@props(['title'=>'Finance'])
<x-erp-layout module="finance" :title="$title">
    <x-slot:assets>

    </x-slot:assets>
    {{$slot}}
</x-erp-layout>
