<?php

namespace Modules\Timekeep\Http\Livewire\Applications;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
        lForm()->setTitle("Loại nghỉ");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.applications"), "Applications");


        return view("timekeep::livewire.applications.index")
            ->layout('timekeep::layouts.master', ['title' => 'Applications']);
    }
}
