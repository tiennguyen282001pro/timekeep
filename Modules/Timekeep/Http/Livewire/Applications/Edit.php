<?php

namespace Modules\Timekeep\Http\Livewire\Applications;

use App\Models\Application;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Edit extends Component
{
    use WithLaravelFormTrait;

    public $name, $salary, $day, $status = '1';

    protected function rules()
    {
        return [
            'name' => 'required',
            'salary' => 'required',
            'day' => '',
            'status' => '',
        ];
    }

    protected function messages()
    {
        return [
            'name.required' => 'Hãy điền tên loại nghỉ',
            'salary.required' => 'Hãy điền phần trăm lương được nhận',
        ];
    }

    public function mount()
    {
        $this->authorize("timekeep.applications.edit");
        $data = Application::findOrFail($this->record_id);
        $this->name = $data->name;
        $this->salary = $data->salary;
        $this->day = $data->day;
        $this->status = $data->status;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("timekeep.applications.edit");
        $this->validate();
        $data = Application::findOrFail($this->record_id);
        $data->fill([
            'name' => $this->name,
            'salary' => $this->salary,
            'day' => $this->day,
            'status' => $this->status,

        ]);
        if (!$data->clean) {
            $data->update();
            return Redirect()->route("timekeep.applications.listing");
        }
    }

    public function render()
    {
        lForm()->setTitle("Cập nhật loại nghỉ");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.applications"), "Applications");
        lForm()->pushBreadcrumb(route("timekeep.applications.edit", $this->record_id), "Edit");

        return view("timekeep::livewire.applications.edit")
            ->layout('timekeep::layouts.master');
    }
}
