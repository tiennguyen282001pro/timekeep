<?php

namespace Modules\Timekeep\Http\Livewire\Applications;

use App\Models\Application;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("timekeep.applications.show");
    }

    public function render()
    {
        $data =  Application::findOrFail($this->record_id);
        lForm()->setTitle("Chi tiết");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.applications"), "Applications");
        lForm()->pushBreadcrumb(route("timekeep.applications.show", $this->record_id), "Show");

        return view("timekeep::livewire.applications.show", compact("data"))
            ->layout('timekeep::layouts.master');
    }
}
