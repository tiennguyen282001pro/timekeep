<?php

namespace Modules\Timekeep\Http\Livewire\Applications;

use App\Models\Application;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class   Create extends Component
{
    use WithLaravelFormTrait;

    public $name, $salary, $day, $status = '1';
    protected $rules = [
        'name' => 'required',
        'salary' => 'required',
        'day' => '',
        'status' => '',
    ];
    protected $messages = [
        'name.required' => 'Hãy điền tên loại nghỉ',
        'salary.required' => 'Hãy điền phần trăm lương được nhận',
    ];

    public function mount()
    {
        $this->authorize("timekeep.applications.create");
        $this->done = 1;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("timekeep.applications.create");
        $this->validate();
        $data = Application::create([
            'name' => $this->name,
            'salary' => $this->salary,
            'day' => $this->day,
            'status' => $this->status,

        ]);
        if ($data) {
            return Redirect()->route("timekeep.applications.listing");
        }
    }

    public function render()
    {
        lForm()->setTitle("Thêm mới loại nghỉ");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.applications"), "Applications");
        lForm()->pushBreadcrumb(route("timekeep.applications.create"), "Create");

        return view("timekeep::livewire.applications.create")
            ->layout('timekeep::layouts.master');
    }
}
