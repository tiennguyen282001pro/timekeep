<?php

namespace Modules\Timekeep\Http\Livewire\Holidays;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
        lForm()->setTitle("Ngày lễ");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.holidays"), "Holidays");


        return view("timekeep::livewire.holidays.index")
            ->layout('timekeep::layouts.master', ['title' => 'Holidays']);
    }
}
