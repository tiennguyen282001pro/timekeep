<?php

namespace Modules\Timekeep\Http\Livewire\Holidays;

use App\Models\Holiday;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Edit extends Component
{
    use WithLaravelFormTrait;

    public $name, $from_day, $to_day, $salary_half, $salary_working, $status;

    protected function rules()
    {
        return [
            'name' => 'required',
            'from_day' => 'required',
            'to_day' => 'required',
            'salary_half' => 'required',
            'salary_working' => 'required',
            'status' => '',

        ];
    }
    protected function messages()
    {
        return [
            'name.required' => 'Hãy điền tên ngày lễ',
            'from_day.required' => 'Hãy điền ngày bắt đầu nghỉ',
            'to_day.required' => 'Hãy điền ngày kết thúc',
            'salary_half.required' => 'Hãy điền lương được hưởng khi nghỉ lễ',
            'salary_working.required' => 'Hãy điền lương được hưởng khi đi làm ngày lễ',
        ];
    }

    public function mount()
    {
        $this->authorize("timekeep.holidays.edit");
        $data = Holiday::findOrFail($this->record_id);
        $this->name = $data->name;
        $this->from_day = $data->from_day;
        $this->to_day = $data->to_day;
        $this->salary_half = $data->salary_half;
        $this->salary_working = $data->salary_working;
        $this->status = $data->status;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("timekeep.holidays.edit");
        $this->validate();
        $data = Holiday::findOrFail($this->record_id);
        $data->fill([
            'name' => $this->name,
            'from_day' => $this->from_day,
            'to_day' => $this->to_day,
            'salary_half' => $this->salary_half,
            'salary_working' => $this->salary_working,
            'status' => $this->status,

        ]);
        if (!$data->clean) {
            $data->update();
            return Redirect()->route("timekeep.holidays.listing");
        }
    }

    public function render()
    {
        lForm()->setTitle("Cập nhật ngày lễ");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.holidays"), "Holidays");
        lForm()->pushBreadcrumb(route("timekeep.holidays.edit", $this->record_id), "Edit");

        return view("timekeep::livewire.holidays.edit")
            ->layout('timekeep::layouts.master');
    }
}
