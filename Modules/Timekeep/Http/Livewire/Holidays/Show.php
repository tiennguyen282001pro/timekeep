<?php

namespace Modules\Timekeep\Http\Livewire\Holidays;

use App\Models\Holiday;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("timekeep.holidays.show");
    }

    public function render()
    {
        $data =  Holiday::findOrFail($this->record_id);
        lForm()->setTitle("Chi tiết");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.holidays"), "Holidays");
        lForm()->pushBreadcrumb(route("timekeep.holidays.show", $this->record_id), "Show");

        return view("timekeep::livewire.holidays.show", compact("data"))
            ->layout('timekeep::layouts.master');
    }
}
