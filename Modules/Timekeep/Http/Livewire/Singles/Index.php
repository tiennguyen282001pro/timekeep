<?php

namespace Modules\Timekeep\Http\Livewire\Singles;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
        lForm()->setTitle("Đơn");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.singles"), "Singles");


        return view("timekeep::livewire.singles.index")
            ->layout('timekeep::layouts.master', ['title' => 'Singles']);
    }
}
