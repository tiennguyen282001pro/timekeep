<?php

namespace Modules\Timekeep\Http\Livewire\Singles;

use App\Models\Application;
use App\Models\Holiday;
use App\Models\Single;
use App\Models\User;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("timekeep.singles.show");
    }

    public function render()
    {
        $data =  Single::findOrFail($this->record_id);
        lForm()->setTitle("Chi tiết");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.singles"), "Singles");
        lForm()->pushBreadcrumb(route("timekeep.singles.show", $this->record_id), "Show");

        // $companies = Company::all();
        $appli = Application::all();
        $users = User::all();
        $holiday = Holiday::all();


        return view("timekeep::livewire.singles.show", compact("data", 'appli', 'users', 'holiday'))
            ->layout('timekeep::layouts.master');
    }
}
