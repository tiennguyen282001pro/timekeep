<?php

namespace Modules\Timekeep\Http\Livewire\TimekeepRules;

use App\Models\TimekeepRule;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\WithPagination;
use Livewire\Component;

class Listing extends Component
{
    use WithLaravelFormTrait;
    use  WithPagination;

    public $confirm = 0;
    // Filter
    public $fId;
    // Sort
    public $sId = 0;
    public $fields = [
        "id" => ["status" => true, "label" => "Id"],
        "name" => ["status" => true, "label" => "Name"],
        "type" => ["status" => true, "label" => "Type"],
        "value" => ["status" => true, "label" => "Value"],
        "status" => ["status" => true, "label" => "Status"],
        "active" => ["status" => true, "label" => "Active"],
        "created_at" => ["status" => true, "label" => "Created At"],
        "updated_at" => ["status" => true, "label" => "Updated At"],

    ];

    public function mount()
    {
        $this->authorize("timekeep.timekeep-rules.listing");
    }

    public function delete()
    {
        $this->authorize("timekeep.timekeep-rules.delete");
        if ($this->confirm > 0) {
            TimekeepRule::destroy($this->confirm);
        }
        $this->confirm = 0;
        $this->dispatchBrowserEvent('warning', 'Timekeep Rules successfully destroyed.');
    }

    public function changeStatus($record_id)
    {
        $data  = TimekeepRule::findOrFail($record_id);

        $data->update([
            "status" => !$data->status
        ]);
    }

    public function changeActive($record_id)
    {
        $data  = TimekeepRule::findOrFail($record_id);

        TimekeepRule::where('active', 1)->update([
            "active" => $data->active
        ]);
        $data->update([
            "active" => !$data->active
        ]);
    }

    public function render()
    {
        $data = new TimekeepRule();

        if ($this->fId > 0) {
            $data = $data->whereId($this->fId);
        }
        if ($this->sId == 1) {
            $data = $data->orderBy("id");
        }
        if ($this->sId == 2) {
            $data = $data->orderByDesc("id");
        }
        $data = $data->paginate(30);

        lForm()->setTitle("Timekeep Rules Listing");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.timekeep-rules"), "Timekeep Rules");
        lForm()->pushBreadcrumb(route("timekeep.timekeep-rules.listing"), "Listing");
        return view("timekeep::livewire.timekeep-rules.listing", compact("data"))
            ->layout('timekeep::layouts.master');
    }
}
