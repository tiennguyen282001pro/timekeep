<?php

namespace Modules\Timekeep\Http\Livewire\TimekeepRules;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
        lForm()->setTitle("Luật chấm công");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.timekeep-rules"), "Timekeep Rules");


        return view("timekeep::livewire.timekeep-rules.index")
            ->layout('timekeep::layouts.master', ['title' => 'Timekeep Rules']);
    }
}
