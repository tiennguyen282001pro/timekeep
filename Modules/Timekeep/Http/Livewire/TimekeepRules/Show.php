<?php

namespace Modules\Timekeep\Http\Livewire\TimekeepRules;

use App\Models\TimekeepRule;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public function mount()
    {
        $this->authorize("timekeep.timekeep-rules.show");
    }

    public function render()
    {
        $data =  TimekeepRule::findOrFail($this->record_id);
        lForm()->setTitle("Chi tiết");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.timekeep-rules"), "Timekeep Rules");
        lForm()->pushBreadcrumb(route("timekeep.timekeep-rules.show", $this->record_id), "Show");

        return view("timekeep::livewire.timekeep-rules.show", compact("data"))
            ->layout('timekeep::layouts.master');
    }
}
