<?php

namespace Modules\Timekeep\Http\Livewire\Timekeeps;

use Livewire\Component;

class Index extends Component
{

    public function render()
    {
       lForm()->setTitle("Timekeeps");
       lForm()->pushBreadcrumb(route("timekeep"),"Timekeep");
		lForm()->pushBreadcrumb(route("timekeep.timekeeps"),"Timekeeps");
		

        return view("timekeep::livewire.timekeeps.index")
            ->layout('timekeep::layouts.master', ['title' => 'Timekeeps']);
    }
}
