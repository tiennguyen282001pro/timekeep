<?php

namespace Modules\Timekeep\Http\Livewire\Timekeeps;

use App\Models\Timekeep;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;

class   Create extends Component
{
    use WithLaravelFormTrait;

    public $user_id, $time, $company_id, $status, $note;
    protected $rules = [
        'user_id' => '',
		'time' => '',
		'company_id' => '',
		'status' => '',
		'note' => '',
		
    ];

    public function mount()
    {
        $this->authorize("timekeep.timekeeps.create");
        $this->done = 1;
    }

    public function updated($field)
    {
        $this->validateOnly($field);
    }

    public function store()
    {
        $this->authorize("timekeep.timekeeps.create");
        $this->validate();
        $data = Timekeep::create([
            'user_id' => $this->user_id,
			'time' => $this->time,
			'company_id' => $this->company_id,
			'status' => $this->status,
			'note' => $this->note,
			
        ]);
        if ($data) {
            $this->redirectForm("timekeep.timekeeps", $data->id);
        }
    }

    public function render()
    {
        lForm()->setTitle("Timekeeps Create");
        lForm()->pushBreadcrumb(route("timekeep"),"Timekeep");
		lForm()->pushBreadcrumb(route("timekeep.timekeeps"),"Timekeeps");
		lForm()->pushBreadcrumb(route("timekeep.timekeeps.create"),"Create");
		
        return view("timekeep::livewire.timekeeps.create")
            ->layout('timekeep::layouts.master');
    }
}
