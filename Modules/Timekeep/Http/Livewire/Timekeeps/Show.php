<?php

namespace Modules\Timekeep\Http\Livewire\Timekeeps;

use App\Models\Holiday;
use App\Models\Single;
use App\Models\Timekeep;
use App\Models\TimeKeepRule;
use App\Models\User;
use Carbon\Carbon;
use Hungnm28\LaravelForm\Traits\WithLaravelFormTrait;
use Livewire\Component;


class Show extends Component
{
    use WithLaravelFormTrait;

    public $month_date;
    public $years_date;

    public function mount()
    {
        $this->authorize("timekeep.timekeeps.show");
        $this->month_date = Carbon::now()->month;
        $this->years_date = Carbon::now()->year;
    }

    public function render()
    {
        $data =  User::findOrFail($this->record_id);
        $month = $this->month_date;
        $timekeeps = Timekeep::whereMonth('created_at', $this->month_date)->whereYear('created_at', $this->years_date)->get();
        $holidays = Holiday::where('status', 1)->get();
        $timekeepRules = TimeKeepRule::all();
        $singles = Single::where('status', 1)->get();
        $user = User::all();

        $staffs = timeKeeps($this->years_date, $this->month_date, $this->record_id, $holidays, $timekeeps, $singles, $timekeepRules, $user);
        $messages = $staffs['mes'];

        lForm()->setTitle("Chi tiết");
        lForm()->pushBreadcrumb(route("timekeep"), "Timekeep");
        lForm()->pushBreadcrumb(route("timekeep.timekeeps"), "Timekeeps");
        lForm()->pushBreadcrumb(route("timekeep.timekeeps.show", $this->record_id), "Show");

        return view("timekeep::livewire.timekeeps.show", compact("data", 'staffs', 'month', 'messages'))
            ->layout('timekeep::layouts.master');
    }
}
