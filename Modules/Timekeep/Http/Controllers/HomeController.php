<?php

namespace Modules\Timekeep\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HomeController extends Controller
{
    public function index()
    {
        lForm()->setTitle('Timekeep');
        lForm()->pushBreadCrumb(route('timekeep'),'Timekeep');

        return view('timekeep::index');
    }
}
