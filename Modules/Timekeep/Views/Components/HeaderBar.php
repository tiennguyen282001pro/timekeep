<?php

namespace Modules\Timekeep\Views\Components;



use Illuminate\View\Component;

class HeaderBar extends Component
{

    public function render(){

        return view('timekeep::components.header-bar');
    }
}
