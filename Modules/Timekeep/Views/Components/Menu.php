<?php

namespace Modules\Timekeep\Views\Components;

use Illuminate\View\Component;

class Menu extends Component
{

    public function render()
    {
        $data = config("timekeep.menu",[]);

        return view("timekeep::components.menu", compact("data"));
    }
}
