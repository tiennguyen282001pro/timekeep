<?php
Route::get('/', [\Modules\Timekeep\Http\Controllers\HomeController::class,"index"])->can('timekeep');

Route::prefix("timekeeps")->name(".timekeeps")->group(function (){
    Route::get("/", \Modules\Timekeep\Http\Livewire\Timekeeps\Index::class)->can("timekeep.timekeeps");
    Route::get("/listing", \Modules\Timekeep\Http\Livewire\Timekeeps\Listing::class)->name(".listing")->can("timekeep.timekeeps.listing");
    Route::get("/create", \Modules\Timekeep\Http\Livewire\Timekeeps\Create::class)->name(".create")->can("timekeep.timekeeps.create");
    Route::get("/edit/{record_id}", \Modules\Timekeep\Http\Livewire\Timekeeps\Edit::class)->name(".edit")->can("timekeep.timekeeps.edit");
    Route::get("/show/{record_id}", \Modules\Timekeep\Http\Livewire\Timekeeps\Show::class)->name(".show")->can("timekeep.timekeeps.show");
    //---END-OF-TIMEKEEPS---//
});

Route::prefix("timekeep-rules")->name(".timekeep-rules")->group(function (){
    Route::get("/", \Modules\Timekeep\Http\Livewire\TimekeepRules\Index::class)->can("timekeep.timekeep-rules");
    Route::get("/listing", \Modules\Timekeep\Http\Livewire\TimekeepRules\Listing::class)->name(".listing")->can("timekeep.timekeep-rules.listing");
    Route::get("/create", \Modules\Timekeep\Http\Livewire\TimekeepRules\Create::class)->name(".create")->can("timekeep.timekeep-rules.create");
    Route::get("/edit/{record_id}", \Modules\Timekeep\Http\Livewire\TimekeepRules\Edit::class)->name(".edit")->can("timekeep.timekeep-rules.edit");
    Route::get("/show/{record_id}", \Modules\Timekeep\Http\Livewire\TimekeepRules\Show::class)->name(".show")->can("timekeep.timekeep-rules.show");
    //---END-OF-TIMEKEEPRULES---//
});

Route::prefix("applications")->name(".applications")->group(function (){
    Route::get("/", \Modules\Timekeep\Http\Livewire\Applications\Index::class)->can("timekeep.applications");
    Route::get("/listing", \Modules\Timekeep\Http\Livewire\Applications\Listing::class)->name(".listing")->can("timekeep.applications.listing");
    Route::get("/create", \Modules\Timekeep\Http\Livewire\Applications\Create::class)->name(".create")->can("timekeep.applications.create");
    Route::get("/edit/{record_id}", \Modules\Timekeep\Http\Livewire\Applications\Edit::class)->name(".edit")->can("timekeep.applications.edit");
    Route::get("/show/{record_id}", \Modules\Timekeep\Http\Livewire\Applications\Show::class)->name(".show")->can("timekeep.applications.show");
    //---END-OF-APPLICATIONS---//
});

Route::prefix("singles")->name(".singles")->group(function (){
    Route::get("/", \Modules\Timekeep\Http\Livewire\Singles\Index::class)->can("timekeep.singles");
    Route::get("/listing", \Modules\Timekeep\Http\Livewire\Singles\Listing::class)->name(".listing")->can("timekeep.singles.listing");
    Route::get("/create", \Modules\Timekeep\Http\Livewire\Singles\Create::class)->name(".create")->can("timekeep.singles.create");
    Route::get("/edit/{record_id}", \Modules\Timekeep\Http\Livewire\Singles\Edit::class)->name(".edit")->can("timekeep.singles.edit");
    Route::get("/show/{record_id}", \Modules\Timekeep\Http\Livewire\Singles\Show::class)->name(".show")->can("timekeep.singles.show");
    //---END-OF-SINGLES---//
});

Route::prefix("holidays")->name(".holidays")->group(function (){
    Route::get("/", \Modules\Timekeep\Http\Livewire\Holidays\Index::class)->can("timekeep.holidays");
    Route::get("/listing", \Modules\Timekeep\Http\Livewire\Holidays\Listing::class)->name(".listing")->can("timekeep.holidays.listing");
    Route::get("/create", \Modules\Timekeep\Http\Livewire\Holidays\Create::class)->name(".create")->can("timekeep.holidays.create");
    Route::get("/edit/{record_id}", \Modules\Timekeep\Http\Livewire\Holidays\Edit::class)->name(".edit")->can("timekeep.holidays.edit");
    Route::get("/show/{record_id}", \Modules\Timekeep\Http\Livewire\Holidays\Show::class)->name(".show")->can("timekeep.holidays.show");
    //---END-OF-HOLIDAYS---//
});

//---END-OF-ROUTES---//
