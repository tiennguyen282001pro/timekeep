<?php
return [
  "menu" => [
    [
      'label' => 'Chấm công',
      'icon' => 'edit',
      'route' => 'timekeep.timekeeps.listing',
      'permission' => 'timekeep.timekeeps.listing',
      'children' => [],
    ],
    [
      'label' => 'Luật chấm công',
      'icon' => 'security',
      'route' => 'timekeep.timekeep-rules',
      'permission' => 'timekeep.timekeep-rules',
      'children' => [],
    ],
    [
      'label' => 'Đơn',
      'icon' => 'contact-mail',
      'route' => 'timekeep.singles',
      'permission' => 'timekeep.singles',
      'children' => [],
    ],
    [
      'label' => 'Loại nghỉ',
      'icon' => 'event',
      'route' => 'timekeep.applications',
      'permission' => 'timekeep.applications',
      'children' => [],
    ],
    [
      'label' => 'Nghỉ lễ',
      'icon' => 'event',
      'route' => 'timekeep.holidays',
      'permission' => 'timekeep.holidays',
      'children' => [],
    ],
  ]
];
