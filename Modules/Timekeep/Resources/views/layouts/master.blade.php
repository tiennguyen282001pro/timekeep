@props(['title'=>'Timekeep'])
<x-erp-layout module="timekeep" :title="$title">
    <x-slot:assets>

    </x-slot:assets>
    {{$slot}}
</x-erp-layout>
