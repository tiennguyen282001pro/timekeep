<div class="w-full flex flex-wrap p-2">
    @can('timekeep.holidays.listing')
        <a href="{{ route('timekeep.holidays.listing') }}" title="Holidays Listing" class="btn-primary xl m-2">
            {!! lfIcon('list', 32) !!}
            <span class="text">Danh sách</span>
        </a>
    @endcan
    @can('timekeep.holidays.create')
        <a href="{{ route('timekeep.holidays.create') }}" title="Holidays Listing" class="btn-success xl m-2">
            {!! lfIcon('add', 32) !!}
            <span class="text">Thêm mới</span>
        </a>
    @endcan
</div>
