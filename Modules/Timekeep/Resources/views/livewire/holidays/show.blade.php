<div class="w-full p-2 md:p-4 max-w-lg">
    <x-lf.card class="success" title="Show">
        <table class="table">
            <tr>
                <th class="text-right pr-2">ID:</th>
                <td>{{ $data->id }}</td>
            </tr>

            <tr>
                <th class="text-right pr-2">Tên ngày lễ:</th>
                <td>{{ $data->name }}</td>
            </tr>
            <tr>
                <th class="text-right pr-2">Nghỉ từ ngày:</th>
                <td>{{ $data->from_day }}</td>
            </tr>
            <tr>
                <th class="text-right pr-2">Đến ngày:</th>
                <td>{{ $data->to_day }}</td>
            </tr>
            <tr>
                <th class="text-right pr-2">Phần trăm lương nghỉ lễ (%):</th>
                <td>{{ $data->salary_half }}</td>
            </tr>
            <tr>
                <th class="text-right pr-2">Phần trăm lương đi làm ngày lễ (%):</th>
                <td>{{ $data->salary_working }}</td>
            </tr>
            <tr>
                <th class="text-right pr-2">Status:</th>
                <td>
                    @if ($data->status == 1)
                        Đang hoạt động
                    @else
                        Không hoạt động
                    @endif
                </td>
            </tr>
        </table>
        <x-slot:tools>
            @can('timekeep.holidays.listing')
                <a class="btn-primary xs" href="{{ route('timekeep.holidays') }}">{!! lfIcon('list', 11) !!}</a>
            @endcan
            @can('timekeep.holidays.edit')
                <a class="btn-warning xs"
                    href="{{ route('timekeep.holidays.edit', $data->id) }}">{!! lfIcon('edit', 11) !!}</a>
            @endcan
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                @can('timekeep.holidays.listing')
                    <a class="btn-primary" href="{{ route('timekeep.holidays') }}">{!! lfIcon('list') !!}
                        <span>Danh sách</span></a>
                @endcan
                <div>
                    @can('timekeep.holidays.edit')
                        <a class="btn-warning"
                            href="{{ route('timekeep.holidays.edit', $data->id) }}">{!! lfIcon('edit') !!}
                            <span>Cập nhật</span></a>
                    @endcan
                </div>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>
