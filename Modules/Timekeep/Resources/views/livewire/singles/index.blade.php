<div class="w-full flex flex-wrap p-2">
    @can('timekeep.singles.listing')
        <a href="{{ route('timekeep.singles.listing') }}" title="Singles Listing" class="btn-primary xl m-2">
            {!! lfIcon('list', 32) !!}
            <span class="text">Danh sách</span>
        </a>
    @endcan
    @can('timekeep.singles.create')
        <a href="{{ route('timekeep.singles.create') }}" title="Singles Listing" class="btn-success xl m-2">
            {!! lfIcon('add', 32) !!}
            <span class="text">Thêm mới</span>
        </a>
    @endcan
</div>
