<div class="w-full flex flex-wrap p-2">
    @can("timekeep.timekeeps.listing")
        <a href="{{route("timekeep.timekeeps.listing")}}" title="Timekeeps Listing" class="btn-primary xl m-2">
            {!! lfIcon("list",32) !!}
            <span class="text">Listing</span>
        </a>
    @endcan
    @can("timekeep.timekeeps.create")
        <a href="{{route("timekeep.timekeeps.create")}}" title="Timekeeps Listing" class="btn-success xl m-2">
            {!! lfIcon("add",32) !!}
            <span class="text">Create</span>
        </a>
    @endcan
</div>
