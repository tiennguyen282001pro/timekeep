<div class="w-full p-1 md:p-4">
    <x-lf.card title="Create" class="info">
        <x-lf.form.input name="user_id" type="integer" label="User id" placeholder="User id ..."/>
		<x-lf.form.input name="time" type="datetime" label="Time" placeholder="Time ..."/>
		<x-lf.form.input name="company_id" type="integer" label="Company id" placeholder="Company id ..."/>
		<x-lf.form.input name="status" type="integer" label="Status" placeholder="Status ..."/>
		<x-lf.form.input name="note" type="string" label="Note" placeholder="Note ..."/>
		
        <x-lf.form.done />
        <x-slot:tools>
            <a class="btn-primary sm" href="{{route('timekeep.timekeeps')}}">{!! lfIcon("list") !!}</a>
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                <label class="btn-primary flex-none" wire:click="store">Create</label>
                <a class="btn" href="{{route("timekeep.timekeeps")}}">Cancel</a>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>



