<div class="w-full flex flex-wrap p-2">
    @can('timekeep.timekeep-rules.listing')
        <a href="{{ route('timekeep.timekeep-rules.listing') }}" title="Timekeep Rules Listing" class="btn-primary xl m-2">
            {!! lfIcon('list', 32) !!}
            <span class="text">Danh sách</span>
        </a>
    @endcan
    @can('timekeep.timekeep-rules.create')
        <a href="{{ route('timekeep.timekeep-rules.create') }}" title="Timekeep Rules Listing" class="btn-success xl m-2">
            {!! lfIcon('add', 32) !!}
            <span class="text">Thêm mới</span>
        </a>
    @endcan
</div>
