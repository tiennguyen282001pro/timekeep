<div class="w-1/2 p-1 md:p-4">
    <x-lf.card title="Cập nhật" class="warning">
        <style>
            input::-webkit-outer-spin-button,
            input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
            }

            / Firefox / input[type=number] {
                -moz-appearance: textfield;
            }
        </style>
        <x-lf.form.input name="name" type="string" onClick="this.select();" style="padding-left:10px">
            <x-slot:label>
                <span>Tên loại nghỉ <span class="text-red-500">(*)</span></span>
            </x-slot:label>
        </x-lf.form.input>
        <div style="display: flex; width:100%">
            <div style=" width:50%">
                <x-lf.form.input name="salary" type="number" onClick="this.select();" style="padding-left:10px">
                    <x-slot:label>
                        <span>Số lương được nhận <span class="text-red-500">(*)</span></span>
                    </x-slot:label>
                </x-lf.form.input>
            </div>
            <div style=" width:50%">
                <x-lf.form.input name="day" type="number" label="Số ngày nghỉ tối đa" />

            </div>
        </div>
        <x-lf.form.toggle name="status" type="integer" label="Trạng thái" />

        <x-slot:tools>
            @can('timekeep.applications.show')
                <a class="btn-success sm"
                    href="{{ route('timekeep.applications.show', $record_id) }}">{!! lfIcon('launch', 11) !!}</a>
            @endcan
            <a class="btn-primary sm" href="{{ route('timekeep.applications') }}">{!! lfIcon('list', 11) !!}</a>
        </x-slot:tools>
        <x-slot:footer>
            <div class="card-footer flex justify-between">
                <label class="btn-primary flex-none" wire:click="store">Cập nhật</label>
                <a class="btn" href="{{ route('timekeep.applications') }}">Hủy bỏ</a>
            </div>
        </x-slot:footer>
    </x-lf.card>
</div>
