<div class="w-full flex flex-wrap p-2">
    @can('timekeep.applications.listing')
        <a href="{{ route('timekeep.applications.listing') }}" title="Applications Listing" class="btn-primary xl m-2">
            {!! lfIcon('list', 32) !!}
            <span class="text">Danh sách</span>
        </a>
    @endcan
    @can('timekeep.applications.create')
        <a href="{{ route('timekeep.applications.create') }}" title="Applications Listing" class="btn-success xl m-2">
            {!! lfIcon('add', 32) !!}
            <span class="text">Thêm mới</span>
        </a>
    @endcan
</div>
