
<header>
    <label for="lf-control" class="h-btn h-menu">{!! lfIcon("menu") !!}</label>
    <div class="pl-4 flex items-center">
        <div class="flex items-center">
            <div class="add_item  bg-white rounded-full text-slate-400 hover:rotate-180" style="transition: 1s" >
                <label for="">{!! lfIcon('add',20) !!}</label>
            </div>
            <div class="pl-4">
                <label for="" class="text-lg text-white">Quản lý {{$module}}</label>
            </div>
        </div>
    </div>
    <div class="header-bar"></div>
{{--begin input search--}}
    <div class="bar-search mr-6"   >
        <div class="h-full">
            <div class="input_search h-full flex items-center justify-center relative " >
                <input type="text" id="search_btn" name="search_btn" placeholder="Tìm kiếm ..." class="px-9 outline-0 w-80 border-0 rounded-md" style="background-color: rgb(175,233,255)" >

                <div class="edit_icon absolute left-2 top-4  " style="color:#484848a8;">
                    <label for="" class=""> {!! lfIcon('search',22) !!}</label>
                </div>
                <div class="icon_tune  absolute right-2  "  x-data="{color :false}"  @click="color =! color" :class="color ? 'text-black': 'text-slate-400'"   style="transition: 1.5s">
                    <label  for="">{!! lfIcon('tune',22) !!}</label>
                </div>

                <div class="list absolute" style="width: 280px ; background-color: #0ea5e9;top: 41px;overflow: auto;box-shadow: 0 0 5px #0ea5e9">
                </div>
            </div>
        </div>
    </div>
    {{--end input search--}}
{{--begin emodule--}}
    <div class="module_show w-12 h-12 relative " x-data="{ open: false }" >
        <div class="open h-full w-full hover:bg-sky-600"   @click="open=!open"    :class="open ? 'bg-sky-600' : ''">
            <label class="loading icon h-full w-full flex cursor-pointer items-center justify-center text-white" >
                {!! lfIcon('app') !!}
            </label>
        </div>
        <div class="box_right absolute right-0 bg-white shadow-2xl "
             x-show="open" x-transition.duration.500ms
             @click.away="open = false"
             style="display: none"  >
            <div class="mounse">
            </div>
            <div class="flex">
                @foreach($ruslt as $key => $item)
                    <div class="w-full py-5  border-r">
                        <h4 class="uppercase font-semibold text-lg ml-11 mt-5 mb-5">
                            @if(!empty($item['title']))
                                {{$item['title']}}
                            @endif
                        </h4>
                        <div class="grid  grid-cols-2 px-4  w-max ">
                            @if(!empty($item['child']))
                                @foreach($item['child'] as $key => $value)
                                    <a href="@if(empty($value->slug))#@else{{ route("$value->slug")}}@endif" class="app-menu-caption inline-flex flex-col text-center items-center  p-4 mb-2 hover:bg-zinc-100 hover:rounded-md" >

                                        <div class="p-2.5 text-sky-50 rounded-md "  style="
                                        background-color:
                                        @if($value->color)
                                        {{$value->color}}
                                        @else
                                        #0ea5e9
                                        @endif
                                        ">
                                            {!! lfIcon($value->icon,25) !!}
                                        </div>
                                        <span class="mt-3 text-sm font-normal" style="max-width: 70px;">{{$value->name}}</span>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endforeach
                <!-- end emodule -->
            </div>
        </div>
    </div>
    <div class="relative " x-data="{mounses: false}" @click="mounses =!mounses">
        <div class="number-notify absolute z-50 text-red-50 px-1   rounded-full bg-red-500" style="font-size: 8px ;top: 0 ;right: 5px;display: none">
            <lable>12</lable>
        </div>
        <div x-show="mounses" @click.away="mounses = false" class="mounse" style="top: 39px;right: 16px;z-index: 1000;display: none" >
        </div>
        <x-lf.btn.dropdown class="h-user " icon="notify">
            <div class="action border-b items-center "
                 x-data="{open1: false,open2: false,open3: false}"
            >
                <div class="">
                    <a  class="text-sm px-1 nav-action"
                        :class="open1 ? 'text-red-500': ' ' "
                    >Công việc</a>
                    <a  :class="open2 ? 'text-red-500': ' ' "  class="text-sm px-1 nav-action">Duyệt</a>
                    <a  :class="open3 ? 'text-red-500': ' ' " class="text-sm px-1 nav-action">Tất cả</a>
                </div>
                <div class="pop control rounded delay-300 duration-300 hover:rounded-full hover:bg-slate-200 ">
                    <lable class=" block p-1  delay-300 duration-300 text-gray-400 hover:text-slate-800 ">{!! lfIcon('more',20) !!}</lable>
                </div>
            </div>
            <div>
                <ul role="list" class="p-6 divide-y divide-slate-200">

                    <!-- Remove top/bottom padding when first/last child -->
                    <li class="flex py-4 first:pt-0 last:pb-0">
                        <img class="h-10 w-10 rounded-full" src="" alt="" />
                        <div class="ml-3 overflow-hidden">
                            <p class="text-sm font-medium text-slate-900"> Nguyễn văn a</p>
                            <p class="text-sm text-slate-500 truncate">đã trả lời vào bài viết của bạn</p>
                            <p class="text-sm text-slate-500 truncate">Khoảng 1 giờ trước</p>
                        </div>
                    </li>
                    <li class="flex py-4 first:pt-0 last:pb-0">
                        <img class="h-10 w-10 rounded-full" src="" alt="" />
                        <div class="ml-3 overflow-hidden">
                            <p class="text-sm font-medium text-slate-900"> Nguyễn văn a</p>
                            <p class="text-sm text-slate-500 truncate">đã trả lời vào bài viết của bạn</p>
                            <p class="text-sm text-slate-500 truncate">đã trả lời vào bài viết của bạn</p>
                        </div>
                    </li>
                    <li class="flex py-4 first:pt-0 last:pb-0">
                        <img class="h-10 w-10 rounded-full" src="" alt="" />
                        <div class="ml-3 overflow-hidden">
                            <p class="text-sm font-medium text-slate-900"> Nguyễn văn a</p>
                            <p class="text-sm text-slate-500 truncate">đã trả lời vào bài viết của bạn</p>
                            <p class="text-sm text-slate-500 truncate">đã trả lời vào bài viết của bạn</p>
                        </div>
                    </li>
                </ul>

                <div>
                    <h3 class="text-center text-gray-400 " > Không có thông báo </h3>
                    <a class=" nav-action block text-center text-gray-900 py-3" > Xem tất cả </a>
                </div>

            </div>
        </x-lf.btn.dropdown>
    </div>

    <x-lf.btn.dropdown class="h-user" icon="person">
        <div class="user">
            <span class="icon">{!! lfIcon('person',56) !!}</span>
            <span class="name">{{Auth::user()->name}}</span>
        </div>
        <div class="action">
            <a class="btn" href="/user/profile">Setting</a>
            <form method="post" action="{{route('logout')}}">
                @csrf
                <button class="btn">Logout</button>
            </form>
        </div>
    </x-lf.btn.dropdown>
</header>

<script>
    let searchBtn = document.getElementById('search_btn');
    let listSearch = document.getElementById('list_searh');

    function callApi()
    {
        fetch("http://127.0.0.1:8000/api/hello")
            .then((response) => response.json())
            .then((data) => {
                handleSearch(data);

            })
    }


    searchBtn.addEventListener("keyup", e=>{
            callApi();
        });
    // searchBtn.addEventListener("keypress", e=>{
    //     if (e.keyCode === 13) {
    //         console.log(1);
    //         alert('Bạn vừa nhấn phím "enter" trong thẻ input');
    //     }
    // });

    function handleSearch(data){
        removeEvents();
        data.forEach((value,index)=>{
            if (value.name.toLowerCase().startsWith(searchBtn.value.toLowerCase()) && searchBtn.value != "") {
                let listItem = document.createElement("li");
                listItem.classList.add('list-items', 'p-3.5','no-underline','list-none','text-base','hover:bg-sky-600');
                listItem.style.cursor = 'pointer';
                listItem.setAttribute("onclick", "displayNames('" + value.name + "')");
                let word = "<a href='" + value.slug + "' >"+"<b>" + value.name.slice(0,searchBtn.value.length) + "</b>"+"</a>";
                word += value.name.slice(searchBtn.value.length)
                listItem.innerHTML = word;
                document.querySelector('.list').appendChild(listItem);
            }
        })
    }
    function displayNames(values)
    {
        searchBtn.value = values;
    }

    function removeEvents()
    {
        const item = document.querySelectorAll(".list-items");
        item.forEach((items)=>{
            items.remove();
        })

    }



</script>

