<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emodules', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable()->collation('utf8mb4_unicode_ci');
            $table->string('label')->nullable()->collation('utf8mb4_unicode_ci');
            $table->string('slug')->nullable()->collation('utf8mb4_unicode_ci');
            $table->string('icon')->nullable()->collation('utf8mb4_unicode_ci');
            $table->string('color')->nullable()->collation('utf8mb4_unicode_ci');
            $table->string('permission')->nullable()->collation('utf8mb4_unicode_ci');
            $table->integer('parent_id')->nullable()->default(0)->index();
            $table->boolean('active')->nullable()->default(0)->index();
            $table->integer('type')->nullable()->default(0)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emodules');
    }
};
